# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8 ai ts=4 sts=4 et sw=4
# Copyright 2012 Neil Muller <drnlmuller+bitbucket@gmail.com>
# GPL - see COPYING for details

"""Simple web-app that dumps the content of a webhook event.

Intended for helping debug / develop the main webhok_proxy app
"""

from flask import Flask, request, render_template_string
from werkzeug.exceptions import BadRequest

app = Flask(__name__)

import json

# defaults - can be overridden in the config file
class DefConfig(object):
    """The default config object."""
    # irker details
    # Address to listen for requests on
    LISTEN = '0.0.0.0'
    PORT = 7550


def process_fail(oExp, oRequest):
    """Print a lot of info about the failure"""
    print(f"Exception: {oExp}")
    print("Headers")
    print(f"{oRequest.headers}")
    print("Data")
    print(f"{oRequest.data}")
    print()
    return render_template_string('<h1>Failed</h1>'), 500


@app.route('/', methods=["POST", "GET"])
def start():
    """The base route - just provides some info."""
    if request.method != 'POST':
        print("Got non-POST request")
        print()
        return render_template_string("<h1>Hello<h1><b>I expected a POST</b>")
    if request.is_json:
        print("Using request.json to get data")
        print()
        print()
        try:
            dWebdata = request.json
        except Exception as oExp:
            print("Failed to decode json from request")
            return process_fail(oExp, request)
    else:
        print("Request isn't marked as json - trying payload")
        print()
        print()
        try:
            dWebdata = json.loads(request.form['payload'])
        except Exception as oExp:
            print("Failed to decode json payload")
            return process_fail(oExp, request)
    print("JSON body %s" % json.dumps(dWebdata, indent=3))
    return render_template_string("<h1>Success</h1>")


if __name__ == "__main__":
    app.config.from_object(DefConfig)
    app.run(host=app.config['LISTEN'], port=app.config['PORT'])
