from setuptools import setup

if __name__ == '__main__':
    setup(name='webhook_proxy',
      description='Bitbucket / github webhook to irker proxy',
      keywords='bitbucket github webhook irker',
      version='0.2.0',
      url='https://bitbucket.org/drnlm/bitbucket-post-to-irker',
      download_url='https://bitbucket.org/drnlm/bitbucket-post-to-irker',
      license='GPL',
      author='Neil Muller',
      author_email='drnlmuller+irker@gmail.com',
      long_description="""
      Waits for webhook calls from bitbucket or github and sends notifications
      to irker.
      """,
      package_dir={'wh_proxy_app': 'wh_proxy_app'},
      packages=['wh_proxy_app', 'wh_proxy_app.tests', 'wh_proxy_app.dump'],
      package_data={'': ['COPYING', 'templates/*html']},
      )
