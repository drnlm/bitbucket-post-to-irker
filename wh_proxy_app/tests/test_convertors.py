"""Test the conversion of various json payloads into irker commit messages"""

import unittest
import json

from ..webhook_proxy import (format_github_pull_request, format_github_push,
                             format_bitbucket_pull_request, format_bitbucket_push,
                             format_gitlab_pull_request, format_gitlab_push,
                             format_sf_push,
                             DefConfig, app)
  

from .data import (GITHUB_PUSH, GITHUB_PR,
                   BITBUCKET_GIT_PUSH, BITBUCKET_HG_PUSH, BITBUCKET_PR, BITBUCKET_WEB_PUSH,
                   GITLAB_PUSH, GITLAB_PR, SF_PUSH)


class TestGithub(unittest.TestCase):

    def setUp(self):
        app.config.from_object(DefConfig)

    def test_pr(self):
        results = format_github_pull_request('wafer', json.loads(GITHUB_PR))
        self.assertEqual(len(results), 1)
        self.assertTrue('drnlm' in results[0])
        self.assertTrue('closed pull request 523' in results[0])
        self.assertTrue('https://github.com/CTPUG/wafer/pull/523' in results[0])

    def test_push(self):
        results = format_github_push('wafer', json.loads(GITHUB_PUSH))
        self.assertEqual(len(results), 10)
        self.assertTrue('feature/support_django_3' in results[0])
        self.assertTrue('https://github.com/CTPUG/wafer/commit/2de5b50ddc4481f319e869590efdf1f9aa51ff86' in results[0])
        self.assertTrue('docs/conf.py setup.py wafer/__init__.py' in results[0])
        self.assertTrue(' Bump version number for next release ' in results[0])

        self.assertTrue('feature/support_django_3' in results[3])
        self.assertTrue('https://github.com/CTPUG/wafer/commit/f098f90bde31b285e5458212afa2df63fdc68431' in results[3])
        self.assertTrue('docs/conf.py setup.py wafer/__init__.py' in results[3])
        self.assertTrue(' Post release version bump ' in results[3])

        self.assertTrue('feature/support_django_3' in results[7])
        self.assertTrue('https://github.com/CTPUG/wafer/commit/2a31603ae5b38b849efaf3649edcc41eb86e1735' in results[7])
        self.assertTrue('etup.py wafer/compare/templates/admin/wafer.compare/change_form.html wafer/pages/templates/wafer.pages/page_form.html wafer/settings.py wafer/talks/templates/wafer.talks/review_talk.html wafer/talks/templates/wafer.talks/talk.html wafer/talks/templates/wafer.talks/talk_form.html' in results[7])
        self.assertTrue('\nNow that django-markitup doesn\'t use inline JS any more (since\n' in results[7])

        self.assertTrue('feature/support_django_3' in results[9])
        self.assertTrue('https://github.com/CTPUG/wafer/commit/e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b' in results[9])
        self.assertTrue('wafer/compare/templates/admin/wafer.compare/change_form.html wafer/pages/templates/wafer.pages/page_form.html wafer/settings.py wafer/talks/templates/wafer.talks/review_talk.html wafer/talks/templates/wafer.talks/talk.html wafer/talks/templates/wafer.talks/talk_form.html' in results[9])
        self.assertTrue(" Merge branch 'master' into feature/support_django_3" in results[9])


class TestBitBucket(unittest.TestCase):

    def setUp(self):
        app.config.from_object(DefConfig)

    def test_pr(self):
        results = format_bitbucket_pull_request(json.loads(BITBUCKET_PR))
        self.assertEqual(len(results), 1)
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue('created' in results[0])
        self.assertTrue('test_pr' in results[0])
        self.assertTrue('master' in results[0])
        self.assertTrue('https://bitbucket.org/drnlm/pipelines_play/pull-requests/1' in results[0])

    def test_git_push(self):
        results = format_bitbucket_push(json.loads(BITBUCKET_GIT_PUSH))
        print(results)
        self.assertTrue('Neil' in results[0])
        self.assertTrue('master' in results[0])
        self.assertTrue('\x024d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c\x0f' in results[0])
        self.assertTrue('Update details' in results[0])
        self.assertTrue('ttps://bitbucket.org/drnlm/pipelines_play/commits/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c' in results[0])

    def test_hg_push(self):
        results = format_bitbucket_push(json.loads(BITBUCKET_HG_PUSH))
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue('\x02ac1e13ceb3feca459342997b4be8ddc6264ee2f8\x0f' in results[0])
        self.assertTrue('default' in results[0])
        self.assertTrue('Add new commit' in results[0])
        self.assertTrue('https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8' in results[0])

    def test_web_push(self):
        results = format_bitbucket_push(json.loads(BITBUCKET_WEB_PUSH))
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue('\x029f3e4312248b5391aa99c29408ab9a7c79715acd\x0f' in results[0])
        self.assertTrue('default' in results[0])
        self.assertTrue('data edited online with Bitbucket' in results[0])
        self.assertTrue('https://bitbucket.org/drnlm/bitbucket-service-test/commits/9f3e4312248b5391aa99c29408ab9a7c79715acd' in results[0])


class TestGitlab(unittest.TestCase):

    def setUp(self):
        app.config.from_object(DefConfig)

    def test_pr(self):
        results = format_gitlab_pull_request(json.loads(GITLAB_PR))
        self.assertEqual(len(results), 1)
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue('opened' in results[0])
        self.assertTrue('pull request 1' in results[0])
        self.assertTrue('test_merge' in results[0])
        self.assertTrue('master' in results[0])
        self.assertTrue('https://gitlab.com/drnlmza/test/-/merge_requests/1' in results[0])

    def test_push(self):
        results = format_gitlab_push(json.loads(GITLAB_PUSH))
        self.assertEqual(len(results), 3)
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue(' do ' in results[0])
        self.assertTrue('\x0245faa2054a0a9bc3021fb6f690533752286cfd2e\x0f' in results[0])
        self.assertTrue('README' in results[0])
        self.assertTrue('https://gitlab.com/drnlmza/test/-/commit/45faa2054a0a9bc3021fb6f690533752286cfd2e' in results[0])

        self.assertTrue('Neil Muller' in results[2])
        self.assertTrue(' re-do? ' in results[2])
        self.assertTrue('\x0240a75a8d6010540fcdec81e1107b33dfff51af3b\x0f' in results[2])
        self.assertTrue('README' in results[2])
        self.assertTrue('https://gitlab.com/drnlmza/test/-/commit/40a75a8d6010540fcdec81e1107b33dfff51af3b' in results[2])


class TestSourceForge(unittest.TestCase):

    def setUp(self):
        app.config.from_object(DefConfig)

    def test_push(self):
        results = format_sf_push(json.loads(SF_PUSH))
        self.assertEqual(len(results), 1)
        self.assertTrue('Neil Muller' in results[0])
        self.assertTrue('Add helper for info dialogs' in results[0])
        self.assertTrue('sutekh/sutekh/base/gui/SutekhDialog.py' in results[0])
        self.assertTrue('sutekh/sutekh/gui/Test.py' in results[0])
        self.assertTrue('sutekh/sutekh/base/gui ' not in results[0])
        self.assertTrue('sutekh/sutekh/gui ' not in results[0])
        self.assertTrue('\x02948c0ca080104658e64f90ecc0b3c6ddf80a32c1\x0f' in results[0])
        self.assertTrue('master' in results[0])
        self.assertTrue('https://sourceforge.net/p/sutekh/sutekh/ci/948c0ca080104658e64f90ecc0b3c6ddf80a32c1/' in results[0])
