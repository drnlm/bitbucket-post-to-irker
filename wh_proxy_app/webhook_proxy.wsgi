# WSGI script for the webhook_proxy
# We assume we're in a virtual env

# Configure these
VIRTUALENV = '/srv/www/webhook'
CONFIG = 'webhook.conf'


import os
import logging

activate_this = os.path.join(VIRTUALENV, 'bin', 'activate_this.py')
execfile(activate_this, dict(__file__=activate_this))

from wh_proxy import app, DefConfig

app.config.from_object(DefConfig)
try:
    app.config.from_pyfile(os.path.join(VIRTUALENV, CONFIG))
except Exception:
    # Ignore failure here
    pass

# Setup seperate logging if required
logfile = app.config['LOG_FILE']
if logfile:
    log_handler = logging.FileHandler(filename=logfile)
    app.logger.addHandler(log_handler)
    if app.config['LOG_INFO']:
        app.logger.setLevel(logging.INFO)
    else:
        app.logger.setLevel(logging.WARNING)

application = app
