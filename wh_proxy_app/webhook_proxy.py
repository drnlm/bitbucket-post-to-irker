# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8 ai ts=4 sts=4 et sw=4
# Copyright 2012 Neil Muller <drnlmuller+bitbucket@gmail.com>
# GPL - see COPYING for details

"""Simple web-app for converting bitbucket or github POST commit calls into
irker notifications"""

import json

from flask import Flask, render_template, request
from werkzeug.exceptions import BadRequest

app = Flask(__name__)

import datetime
import json
import socket
import logging
import sys
import traceback

# IRC colour defaults
MIRC_COLOURS = {
    'bold': '\x02',
    'green': '\x0303',
    'blue': '\x0302',
    'red': '\x0305',
    'yellow': '\x0307',
    'brown': '\x0305',
    'magenta': '\x0306',
    'cyan': '\x0310',
    'reset': '\x0F',
    }

NO_COLOURS = {
    'bold': '',
    'green': '',
    'blue': '',
    'red': '',
    'yellow': '',
    'brown': '',
    'magenta': '',
    'cyan': '',
    'reset': '',
    }


# defaults - can be overridden in the config file
class DefConfig(object):
    """The default config object."""
    # irker details
    IRKER_SERVER = 'localhost'
    IRKER_PORT = 6659
    # Address to listen for requests on
    LISTEN = '0.0.0.0'
    PORT = 5550
    # debugging on?
    DEBUG = False
    # List of channels to post to if bot using SPECIFY_CHANNEL_IN_URL
    CHANNELS = []
    # Are channels given in the url?
    SPECIFY_CHANNEL_IN_URL = False
    # channels to allow posting to if SPECIFY_CHANNEL_IN_URL is True
    # Format {'channel_name': 'channel_url'}
    ALLOWED_CHANNELS = {}
    COLOURS = True
    # Show urls in commit message
    URLS = True
    LOG_ERRORS = False
    LOG_INFO = False
    LOG_FILE = None


# Utilty functions

def process_channels(sChannel):
    """Return the channels to post to, either from the
       request, or the config file, as specified."""
    if app.config['SPECIFY_CHANNEL_IN_URL']:
        # Channel needs to be from the url
        if not sChannel:
            # No channel, so error out
            return None
        dAllowedChannels = app.config['ALLOWED_CHANNELS']
        if not dAllowedChannels:
            # Nothing to do
            if app.config['LOG_INFO']:
                app.logger.info('No channels allowed. Aborting')
            return
        aChannels = [x.strip() for x in sChannel.split(',')]
        bOK = True
        aFullChannels = []
        for sChannel in aChannels:
            if sChannel not in dAllowedChannels:
                # Log the error
                app.logger.error('Tried to send to a channel not on the'
                                 ' allowed list: %s' % sChannel)
                # Flag that we're not posting
                bOK = False
            else:
                aFullChannels.append(dAllowedChannels[sChannel])
        if not bOK:
            return None
        if app.config['LOG_INFO']:
            app.logger.info('Allowed channel names: %s' %
                            ' - '.join(dAllowedChannels))
            app.logger.info('Requested channels: %s' % ' - '.join(aChannels))
            app.logger.info(
                'Full channel uris: %s' % ' - '.join(aFullChannels))
        return aFullChannels
    else:
        # Get channel from config file
        aChannels = app.config['CHANNELS']
        return aChannels


def format_info(sProject, sAuthor, sCommitMessage, sBranch, sRev, aFiles,
                sUrl):
    """Format the commit info into a irc message to feed to irker"""
    # We truncate the file list and commit messages to 'reasonable' lengths
    # here
    dInfo = {'project': sProject,
             'committer': sAuthor,
             'files': ' '.join(aFiles)[:300],
             'branch': sBranch,
             'rev': sRev,
             'msg': sCommitMessage[:300],
             'url': sUrl,
             }
    if app.config['COLOURS']:
        dInfo.update(MIRC_COLOURS)
    else:
        dInfo.update(NO_COLOURS)
    sMsg = ('%(bold)s%(project)s:%(reset)s '
            '%(green)s%(committer)s%(reset)s '
            '%(yellow)s%(branch)s%(reset)s * %(bold)s%(rev)s%(reset)s / '
            '%(bold)s%(files)s%(reset)s: %(msg)s '
            '%(brown)s%(url)s%(reset)s' % dInfo)
    return sMsg


def send(aChannels, sCommit):
    """Send a message to irker"""
    sServer = app.config['IRKER_SERVER']
    iPort = int(app.config['IRKER_PORT'])
    oSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        # We truncate sCommit to 450, and hope that's within the
        # irc server limit
        sMsg = json.dumps({"to": aChannels, "privmsg": sCommit[:450]})
        if app.config['LOG_INFO']:
            app.logger.info('sent to irkerd: %s' % sMsg)
        oSock.connect((sServer, iPort))
        # Send as bytes
        oSock.sendall(sMsg.encode('utf8') + b"\n")
    except socket.error as oExp:
        if app.config['LOG_ERRORS']:
            app.logger.error('Socket error: %s' % oExp)
    finally:
        oSock.close()

# Hosting specific utility functions


def format_github_pull_request(sProject, dWebData):
    """Format a pull request from github into a irc message."""
    sPullRequest = "pull request %d (%s)" % (
        dWebData['number'], dWebData['pull_request']['title'])
    sAction = dWebData['action']
    # Slightly better text
    if sAction == 'synchronize':
        sAction = 'updated'
    sUrl = dWebData['pull_request']['html_url']
    sAuthor = dWebData['sender']['login']
    sHead = dWebData['pull_request']['head']['label']
    sTarget = dWebData['pull_request']['base']['label']
    dInfo = {'project': sProject,
             'committer': sAuthor,
             'action': sAction,
             'request': sPullRequest,
             'url': sUrl,
             'head': sHead,
             'target': sTarget,
             }
    if app.config['COLOURS']:
        dInfo.update(MIRC_COLOURS)
    else:
        dInfo.update(NO_COLOURS)
    sMsg = ('%(bold)s%(project)s:%(reset)s '
            '%(green)s%(committer)s%(reset)s '
            '%(yellow)s%(action)s %(request)s%(reset)s '
            'from %(bold)s%(head)s%(reset)s '
            'into %(bold)s%(target)s%(reset)s '
            '%(brown)s%(url)s%(reset)s'
            % dInfo)
    return [sMsg]


def format_github_push(sProject, dWebData):
    """Format a git push message from github."""
    aMessages = []
    sBranch = dWebData['ref'].replace('refs/heads/', '')
    for dCommit in dWebData['commits']:
        sAuthor = dCommit['committer']['name']
        # Files should be the combination of added, modified + removed
        aFiles = dCommit['modified']
        aFiles.extend(['+%s' % x for x in dCommit['added']])
        aFiles.extend(['-%s' % x for x in dCommit['removed']])
        sCommitMessage = dCommit['message']
        sRev = dCommit['id']  # Shorten this?
        if app.config['URLS']:
            sUrl = dCommit['url']
        else:
            sUrl = ''
        sMsg = format_info(sProject, sAuthor, sCommitMessage,
                           sBranch, sRev, aFiles, sUrl)
        aMessages.append(sMsg)
    return aMessages



def format_bitbucket_pull_request(dWebData):
    """Format a bitbucket PR event as an irker message"""
    sProject = dWebData['repository']['name']
    sPullRequest = "pull request %d (%s)" % (
            dWebData['pullrequest']['id'], dWebData['pullrequest']['title'])
    sState = dWebData['pullrequest']['state']
    # We just want a relative difference, so we discard the timezone and milliseconds
    oUpdated = datetime.datetime.strptime(dWebData['pullrequest']['updated_on'][:19],
                                          '%Y-%m-%dT%H:%M:%S')
    oCreated = datetime.datetime.strptime(dWebData['pullrequest']['created_on'][:19],
                                          '%Y-%m-%dT%H:%M:%S')
    # Updated and created are never equal in my tests, so we need a margin here

    if sState == 'OPEN':
        if (oUpdated - oCreated) < datetime.timedelta(seconds=5):
            sAction = 'created'
        else:
            sAction = 'updated'
    elif sState == 'MERGED':
        sAction = 'merged'
    else:
        sAction = 'closed (no merge)'
    sUrl = dWebData['pullrequest']['links']['html']['href']
    sAuthor = dWebData['actor']['display_name']
    sSource = dWebData['pullrequest']['source']['branch']['name']
    sTarget = dWebData['pullrequest']['destination']['branch']['name']
    dInfo = {'project': sProject,
             'committer': sAuthor,
             'action': sAction,
             'request': sPullRequest,
             'url': sUrl,
             'source': sSource,
             'target': sTarget,
             }
    if app.config['COLOURS']:
        dInfo.update(MIRC_COLOURS)
    else:
        dInfo.update(NO_COLOURS)
    sMsg = ('%(bold)s%(project)s:%(reset)s '
            '%(green)s%(committer)s%(reset)s '
            '%(yellow)s%(action)s %(request)s%(reset)s '
            'from %(bold)s%(source)s%(reset)s '
            'into %(bold)s%(target)s%(reset)s '
            '%(brown)s%(url)s%(reset)s'
            % dInfo)
    return [sMsg]


def format_bitbucket_push(dWebData):
    """Format the bitbucket json blob into an irc message"""
    aMessages = []
    sProject = dWebData['repository']['name']
    for dChange in dWebData['push']['changes']:
        sBranch = dChange['new']['name']
        for dCommit in dChange['commits']:
            if 'user' in dCommit['author']:
                sAuthor = dCommit['author']['user']['display_name']
            else:
                # Raw, we assume
                if ' <' in dCommit['author']['raw']:
                    # Grab the bit before the email address
                    sAuthor = dCommit['author']['raw'].split(' <', 1)[0]
                else:
                    # No better idea
                    sAuthor = dCommit['author']['raw']
            sCommitMessage = dCommit['message'].strip()
            sRev = '%s' % dCommit['hash']
            # New bitbucket doesn't expose this in the push event
            aFiles = []
            if app.config['URLS']:
                sUrl = dCommit['links']['html']['href']
            else:
                sUrl = ''
            sMsg = format_info(sProject, sAuthor, sCommitMessage,
                               sBranch, sRev, aFiles, sUrl)
            aMessages.append(sMsg)
    return aMessages


def format_gitlab_pull_request(dWebData):
    """format a gitlab PR as an irker message"""
    sProject = dWebData['repository']['name']
    sPullRequest = "pull request %d (%s)" % (
            dWebData['object_attributes']['iid'], dWebData['object_attributes']['description'])
    sAction = dWebData['object_attributes']['state']
    sUrl = dWebData['object_attributes']['url']
    sAuthor = dWebData['user']['name']
    sSource = dWebData['object_attributes']['source_branch']
    sTarget = dWebData['object_attributes']['target_branch']
    dInfo = {'project': sProject,
             'committer': sAuthor,
             'action': sAction,
             'request': sPullRequest,
             'url': sUrl,
             'source': sSource,
             'target': sTarget,
             }
    if app.config['COLOURS']:
        dInfo.update(MIRC_COLOURS)
    else:
        dInfo.update(NO_COLOURS)
    sMsg = ('%(bold)s%(project)s:%(reset)s '
            '%(green)s%(committer)s%(reset)s '
            '%(yellow)s%(action)s %(request)s%(reset)s '
            'from %(bold)s%(source)s%(reset)s '
            'into %(bold)s%(target)s%(reset)s '
            '%(brown)s%(url)s%(reset)s'
            % dInfo)
    return [sMsg]


def format_gitlab_push(dWebData):
    """Format a gitlab push event as a set of irker messages"""
    aMessages = []
    sProject = dWebData['repository']['name']
    sBranch = dWebData['ref'].replace('/ref/heads', '')
    for dCommit in dWebData['commits']:
        sAuthor = dCommit['author']['name']
        sCommitMessage = dCommit['message'].strip()
        sRev = '%s' % dCommit['id']
        aFiles = dCommit['modified']
        aFiles.extend(['+%s' % x for x in dCommit['added']])
        aFiles.extend(['-%s' % x for x in dCommit['removed']])
        if app.config['URLS']:
            sUrl = dCommit['url']
        else:
            sUrl = ''
        sMsg = format_info(sProject, sAuthor, sCommitMessage,
                           sBranch, sRev, aFiles, sUrl)
        aMessages.append(sMsg)
    return aMessages


def format_sf_push(dWebData):
    """Format a sourceforge push event as a set of irker messages"""
    aMessages = []
    sProject = dWebData['repository']['name']
    sBranch = dWebData['ref'].replace('/ref/heads', '')
    for dCommit in dWebData['commits']:
        sAuthor = dCommit['author']['name']
        sCommitMessage = dCommit['message'].strip()
        sRev = '%s' % dCommit['id']
        aFiles = dCommit['modified']
        aFiles.extend(['+%s' % x for x in dCommit['added']])
        aFiles.extend(['-%s' % x for x in dCommit['removed']])
        aFiltered = []
        for x in aFiles:
            found = len([y for y in aFiles if x in y])
            if found > 1:
                continue
            aFiltered.append(x)
        if app.config['URLS']:
            sUrl = dCommit['url']
        else:
            sUrl = ''
        sMsg = format_info(sProject, sAuthor, sCommitMessage,
                           sBranch, sRev, aFiltered, sUrl)
        aMessages.append(sMsg)
    # Sourceforge has the commits in the opposite order from
    # most other systems
    return reversed(aMessages)



# Flask routing functions

@app.route('/')
def start():
    """The base route - just provides some info."""
    return render_template('index.html')


@app.route('/bitbucket/<sChannel>/<sProject>', methods=['POST'])
@app.route('/bitbucket/<sProject>', methods=['POST'])
def bitbucket_hook(sProject, sChannel=None):
    """Turn a bitbucket POST hook into an irker message"""
    aChannels = process_channels(sChannel)
    if not aChannels:
        if app.config['LOG_ERRORS']:
            app.logger.error('Unable to determine channels to post to')
        return render_template('fail.html')
    try:
        dWebData = request.json
        if app.config['LOG_INFO']:
            app.logger.info('bitbucket payload: %s'
                            % json.dumps(dWebData, indent=3))
        if 'pullrequest' in dWebData:
            aMessages = format_bitbucket_pr(dWebData)
        elif 'push' in dWebData:
            aMessages = format_bitbucket_push(dWebData)
        else:
            if app.config['LOG_ERRORS']:
                app.logger.error('Unrecognised json blob')
            return render_template('fail.html')
        for sMsg in aMessages:
            send(aChannels, sMsg)
    except Exception as oExp:
        if app.config['LOG_ERRORS']:
            app.logger.error('Exception: %s' % oExp)
            aTraceback = traceback.format_exception(*sys.exc_info())
            app.logger.error('Details: %s' % "".join(aTraceback))
            app.logger.error('Request headers: %s' % request.headers)
            app.logger.error('Request data: %s' % request.data)
        # doesn't look like a bitbucket web call
        return render_template('fail.html')
    return render_template('post.html')


@app.route('/github/<sChannel>/<sProject>', methods=['POST'])
@app.route('/github/<sProject>', methods=['POST'])
def github_hook(sProject, sChannel=None):
    """Turn a github webhook into an irker message"""
    aChannels = process_channels(sChannel)
    if not aChannels:
        if app.config['LOG_ERRORS']:
            app.logger.error('Unable to determine channels to post to')
        return render_template('fail.html')
    try:
        if request.is_json:
            dWebData = request.json
        else:
            try:
                dWebData = json.loads(request.form['payload'])
            except BadRequest:
                app.logger.error('Failed to load json payload')
                return render_template('fail.html')
        if app.config['LOG_INFO']:
            app.logger.info('github payload: %s'
                            % json.dumps(dWebData, indent=3))
        if 'pull_request' in dWebData:
            aMessages = format_github_pull_request(sProject, dWebData)
        elif 'commits' in dWebData:
            aMessages = format_github_push(sProject, dWebData)
        else:
            if app.config['LOG_ERRORS']:
                app.logger.error('Unrecognised json blob')
            return render_template('fail.html')
        for sMsg in aMessages:
            send(aChannels, sMsg)
    except Exception as oExp:
        if app.config['LOG_ERRORS']:
            app.logger.error('Exception: %s' % oExp)
            aTraceback = traceback.format_exception(*sys.exc_info())
            app.logger.error('Details: %s' % "".join(aTraceback))
            app.logger.error('Request headers: %s' % request.headers)
            app.logger.error('Request data: %s' % request.data)
        # doesn't look like a github web call
        return render_template('fail.html')
    return render_template('post.html')


@app.route('/gitlab/<sChannel>/<sProject>', methods=['POST'])
@app.route('/gitlab/<sProject>', methods=['POST'])
def gitlab_hook(sProject, sChannel=None):
    """Turn a gitlab webhook into an irker message"""
    aChannels = process_channels(sChannel)
    if not aChannels:
        if app.config['LOG_ERRORS']:
            app.logger.error('Unable to determine channels to post to')
        return render_template('fail.html')
    try:
        dWebData = request.json
        if app.config['LOG_INFO']:
            app.logger.info('gitlab payload: %s'
                            % json.dumps(dWebData, indent=3))
        if dWebData['object_kind'] == 'merge_request':
            aMessages = format_gitlab_pull_request()
        elif 'commits' in dWebData:
            aMessages = format_gitlab_push(dWebData)
        else:
            if app.config['LOG_ERRORS']:
                app.logger.error('Unrecognised json blob')
            return render_template('fail.html')
        for sMsg in aMessages:
            send(aChannels, sMsg)
    except Exception as oExp:
        if app.config['LOG_ERRORS']:
            app.logger.error('Exception: %s' % oExp)
            aTraceback = traceback.format_exception(*sys.exc_info())
            app.logger.error('Details: %s' % "".join(aTraceback))
            app.logger.error('Request headers: %s' % request.headers)
            app.logger.error('Request data: %s' % request.data)
        # doesn't look like a gitlab web call
        return render_template('fail.html')
    return render_template('post.html')


@app.route('/sf/<sChannel>/<sProject>', methods=['POST'])
@app.route('/sf/<sProject>', methods=['POST'])
def sf_hook(sProject, sChannel=None):
    """Turn a sourceforge webhook into an irker message"""
    aChannels = process_channels(sChannel)
    if not aChannels:
        if app.config['LOG_ERRORS']:
            app.logger.error('Unable to determine channels to post to')
        return render_template('fail.html')
    try:
        dWebData = request.json
        if app.config['LOG_INFO']:
            app.logger.info('sf payload: %s'
                            % json.dumps(dWebData, indent=3))
        aMessages = format_sf_push(dWebData)
        for sMsg in aMessages:
            send(aChannels, sMsg)
    except Exception as oExp:
        if app.config['LOG_ERRORS']:
            app.logger.error('Exception: %s' % oExp)
            aTraceback = traceback.format_exception(*sys.exc_info())
            app.logger.error('Details: %s' % "".join(aTraceback))
            app.logger.error('Request headers: %s' % request.headers)
            app.logger.error('Request data: %s' % request.data)
        # doesn't look like a gitlab web call
        return render_template('fail.html')
    return render_template('post.html')


if __name__ == "__main__":
    app.config.from_object(DefConfig)
    try:
        app.config.from_envvar('WEBHOOK_CONFIG')
    except RuntimeError:
        # Ignore the env var not being set
        pass
    bDebug = app.config['DEBUG']
    sLogFile = app.config['LOG_FILE']
    if sLogFile:
        oLogHandler = logging.FileHandler(filename=sLogFile)
        app.logger.addHandler(oLogHandler)
        if app.config['LOG_INFO']:
            app.logger.setLevel(logging.INFO)
        else:
            app.logger.setLevel(logging.WARNING)
    if bDebug:
        app.run()
    else:
        app.run(host=app.config['LISTEN'], port=app.config['PORT'])
