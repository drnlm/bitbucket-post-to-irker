"""Define constants for our test cases, to keep that file more readable"""


# Push to the wafer repo
GITHUB_PUSH = u"""
{
   "forced": false, 
   "compare": "https://github.com/CTPUG/wafer/compare/c4138096f9b9...e5fbad190b41", 
   "sender": {
      "following_url": "https://api.github.com/users/drnlm/following{/other_user}", 
      "events_url": "https://api.github.com/users/drnlm/events{/privacy}", 
      "avatar_url": "https://avatars2.githubusercontent.com/u/642056?v=4", 
      "url": "https://api.github.com/users/drnlm", 
      "gists_url": "https://api.github.com/users/drnlm/gists{/gist_id}", 
      "html_url": "https://github.com/drnlm", 
      "subscriptions_url": "https://api.github.com/users/drnlm/subscriptions", 
      "node_id": "MDQ6VXNlcjY0MjA1Ng==", 
      "repos_url": "https://api.github.com/users/drnlm/repos", 
      "received_events_url": "https://api.github.com/users/drnlm/received_events", 
      "gravatar_id": "", 
      "starred_url": "https://api.github.com/users/drnlm/starred{/owner}{/repo}", 
      "site_admin": false, 
      "login": "drnlm", 
      "type": "User", 
      "id": 642056, 
      "followers_url": "https://api.github.com/users/drnlm/followers", 
      "organizations_url": "https://api.github.com/users/drnlm/orgs"
   }, 
   "repository": {
      "issues_url": "https://api.github.com/repos/CTPUG/wafer/issues{/number}", 
      "deployments_url": "https://api.github.com/repos/CTPUG/wafer/deployments", 
      "stargazers_count": 33, 
      "forks_url": "https://api.github.com/repos/CTPUG/wafer/forks", 
      "mirror_url": null, 
      "subscription_url": "https://api.github.com/repos/CTPUG/wafer/subscription", 
      "notifications_url": "https://api.github.com/repos/CTPUG/wafer/notifications{?since,all,participating}", 
      "collaborators_url": "https://api.github.com/repos/CTPUG/wafer/collaborators{/collaborator}", 
      "updated_at": "2020-03-27T00:29:51Z", 
      "private": false, 
      "pulls_url": "https://api.github.com/repos/CTPUG/wafer/pulls{/number}", 
      "disabled": false, 
      "issue_comment_url": "https://api.github.com/repos/CTPUG/wafer/issues/comments{/number}", 
      "labels_url": "https://api.github.com/repos/CTPUG/wafer/labels{/name}", 
      "has_wiki": true, 
      "full_name": "CTPUG/wafer", 
      "owner": {
         "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
         "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
         "name": "CTPUG", 
         "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
         "url": "https://api.github.com/users/CTPUG", 
         "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
         "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
         "html_url": "https://github.com/CTPUG", 
         "email": null, 
         "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
         "repos_url": "https://api.github.com/users/CTPUG/repos", 
         "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
         "gravatar_id": "", 
         "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
         "site_admin": false, 
         "login": "CTPUG", 
         "type": "Organization", 
         "id": 647522, 
         "followers_url": "https://api.github.com/users/CTPUG/followers", 
         "organizations_url": "https://api.github.com/users/CTPUG/orgs"
      }, 
      "statuses_url": "https://api.github.com/repos/CTPUG/wafer/statuses/{sha}", 
      "id": 8658619, 
      "keys_url": "https://api.github.com/repos/CTPUG/wafer/keys{/key_id}", 
      "description": "A wafer-thin web application for running small conferences. Built using Django.", 
      "tags_url": "https://api.github.com/repos/CTPUG/wafer/tags", 
      "archived": false, 
      "downloads_url": "https://api.github.com/repos/CTPUG/wafer/downloads", 
      "assignees_url": "https://api.github.com/repos/CTPUG/wafer/assignees{/user}", 
      "contents_url": "https://api.github.com/repos/CTPUG/wafer/contents/{+path}", 
      "has_pages": false, 
      "git_refs_url": "https://api.github.com/repos/CTPUG/wafer/git/refs{/sha}", 
      "open_issues_count": 27, 
      "has_projects": true, 
      "clone_url": "https://github.com/CTPUG/wafer.git", 
      "watchers_count": 33, 
      "git_tags_url": "https://api.github.com/repos/CTPUG/wafer/git/tags{/sha}", 
      "milestones_url": "https://api.github.com/repos/CTPUG/wafer/milestones{/number}", 
      "languages_url": "https://api.github.com/repos/CTPUG/wafer/languages", 
      "size": 1974, 
      "homepage": null, 
      "fork": false, 
      "commits_url": "https://api.github.com/repos/CTPUG/wafer/commits{/sha}", 
      "releases_url": "https://api.github.com/repos/CTPUG/wafer/releases{/id}", 
      "issue_events_url": "https://api.github.com/repos/CTPUG/wafer/issues/events{/number}", 
      "archive_url": "https://api.github.com/repos/CTPUG/wafer/{archive_format}{/ref}", 
      "comments_url": "https://api.github.com/repos/CTPUG/wafer/comments{/number}", 
      "events_url": "https://api.github.com/repos/CTPUG/wafer/events", 
      "contributors_url": "https://api.github.com/repos/CTPUG/wafer/contributors", 
      "html_url": "https://github.com/CTPUG/wafer", 
      "forks": 21, 
      "compare_url": "https://api.github.com/repos/CTPUG/wafer/compare/{base}...{head}", 
      "open_issues": 27, 
      "node_id": "MDEwOlJlcG9zaXRvcnk4NjU4NjE5", 
      "git_url": "git://github.com/CTPUG/wafer.git", 
      "svn_url": "https://github.com/CTPUG/wafer", 
      "merges_url": "https://api.github.com/repos/CTPUG/wafer/merges", 
      "has_issues": true, 
      "ssh_url": "git@github.com:CTPUG/wafer.git", 
      "blobs_url": "https://api.github.com/repos/CTPUG/wafer/git/blobs{/sha}", 
      "master_branch": "master", 
      "git_commits_url": "https://api.github.com/repos/CTPUG/wafer/git/commits{/sha}", 
      "hooks_url": "https://api.github.com/repos/CTPUG/wafer/hooks", 
      "has_downloads": true, 
      "license": {
         "spdx_id": "ISC", 
         "url": "https://api.github.com/licenses/isc", 
         "node_id": "MDc6TGljZW5zZTEw", 
         "name": "ISC License", 
         "key": "isc"
      }, 
      "name": "wafer", 
      "language": "Python", 
      "url": "https://github.com/CTPUG/wafer", 
      "stargazers": 33, 
      "created_at": 1362772258, 
      "watchers": 33, 
      "pushed_at": 1586257570, 
      "forks_count": 21, 
      "default_branch": "master", 
      "teams_url": "https://api.github.com/repos/CTPUG/wafer/teams", 
      "trees_url": "https://api.github.com/repos/CTPUG/wafer/git/trees{/sha}", 
      "organization": "CTPUG", 
      "branches_url": "https://api.github.com/repos/CTPUG/wafer/branches{/branch}", 
      "subscribers_url": "https://api.github.com/repos/CTPUG/wafer/subscribers", 
      "stargazers_url": "https://api.github.com/repos/CTPUG/wafer/stargazers"
   }, 
   "pusher": {
      "name": "drnlm", 
      "email": "drnlmuller+github@gmail.com"
   }, 
   "deleted": false, 
   "commits": [
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-07T16:51:19+02:00", 
         "modified": [
            "docs/conf.py", 
            "setup.py", 
            "wafer/__init__.py"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/2de5b50ddc4481f319e869590efdf1f9aa51ff86", 
         "tree_id": "2aa6f3eb5edf7f40b9e76810aae365d57a1576c8", 
         "message": "Bump version number for next release", 
         "removed": [], 
         "id": "2de5b50ddc4481f319e869590efdf1f9aa51ff86"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-07T16:53:03+02:00", 
         "modified": [
            ".travis.yml", 
            "README.rst"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/0ad3dc243c835e40d19dd8170829a519df7c6e1f", 
         "tree_id": "17d4ccab15b0c2ae5417eef63a989bb0d8242ec5", 
         "message": "Mark Django 2.1 and 2.2 as supported", 
         "removed": [], 
         "id": "0ad3dc243c835e40d19dd8170829a519df7c6e1f"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-07T17:09:21+02:00", 
         "modified": [
            "LICENSE", 
            "docs/conf.py"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/7614af110a162d35b8a16c6b690bfda548f8c818", 
         "tree_id": "c031c2ec7bb57a1fba6dcd54e2d3e246e74a3af6", 
         "message": "Update years", 
         "removed": [], 
         "id": "7614af110a162d35b8a16c6b690bfda548f8c818"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-07T17:10:37+02:00", 
         "modified": [
            "docs/conf.py", 
            "setup.py", 
            "wafer/__init__.py"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/f098f90bde31b285e5458212afa2df63fdc68431", 
         "tree_id": "03cc8e996751b11da108571ed4fe3980bf817ff1", 
         "message": "Post release version bump", 
         "removed": [], 
         "id": "f098f90bde31b285e5458212afa2df63fdc68431"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": true, 
         "timestamp": "2020-03-20T22:21:34+02:00", 
         "modified": [
            "LICENSE", 
            "README.rst", 
            "docs/conf.py", 
            "setup.py", 
            "wafer/__init__.py"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/9e9f2c442f064503c64a8c8a30bdb41c0e28557d", 
         "tree_id": "ce9bdc8599afdfd916a768730ad9b8057839de36", 
         "message": "Merge branch 'master' into feature/support_django_3", 
         "removed": [], 
         "id": "9e9f2c442f064503c64a8c8a30bdb41c0e28557d"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": true, 
         "timestamp": "2020-03-20T22:22:32+02:00", 
         "modified": [
            "setup.py"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/5bdd2b1369b64c6543261c015727e3e4875f0185", 
         "tree_id": "d57cedd8af2dcb606d2e5441deb1d559374871ad", 
         "message": "Fix django-markitup requirement", 
         "removed": [], 
         "id": "5bdd2b1369b64c6543261c015727e3e4875f0185"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": true, 
         "timestamp": "2020-03-20T22:25:46+02:00", 
         "modified": [
            "wafer/templates/wafer/base_form.html"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/773b00f9f17794980c39639be92f96ed56c9b76b", 
         "tree_id": "a8a5c1b639aaf057c01a19a385172dccaa950ba1", 
         "message": "Fix template", 
         "removed": [], 
         "id": "773b00f9f17794980c39639be92f96ed56c9b76b"
      }, 
      {
         "committer": {
            "username": "stefanor", 
            "name": "Stefano Rivera", 
            "email": "stefano@rivera.za.net"
         }, 
         "added": [], 
         "author": {
            "username": "stefanor", 
            "name": "Stefano Rivera", 
            "email": "stefano@rivera.za.net"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-26T14:49:56-07:00", 
         "modified": [
            "setup.py", 
            "wafer/compare/templates/admin/wafer.compare/change_form.html", 
            "wafer/pages/templates/wafer.pages/page_form.html", 
            "wafer/settings.py", 
            "wafer/talks/templates/wafer.talks/review_talk.html", 
            "wafer/talks/templates/wafer.talks/talk.html", 
            "wafer/talks/templates/wafer.talks/talk_form.html"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/2a31603ae5b38b849efaf3649edcc41eb86e1735", 
         "tree_id": "5d1ab276b36322eb59cf1878d0786cea25f880ee", 
         "message": "Revert markitup hacks\\n\\nNow that django-markitup doesn't use inline JS any more (since\\nCTPUG/django-markitup#9, in 3.7.0)", 
         "removed": [
            "wafer/sponsors/templates/admin/sponsors/change_form.html", 
            "wafer/static/js/markitup.js", 
            "wafer/templates/markitup/editor.html"
         ], 
         "id": "2a31603ae5b38b849efaf3649edcc41eb86e1735"
      }, 
      {
         "committer": {
            "username": "web-flow", 
            "name": "GitHub", 
            "email": "noreply@github.com"
         }, 
         "added": [], 
         "author": {
            "username": "stefanor", 
            "name": "Stefano Rivera", 
            "email": "stefano@rivera.za.net"
         }, 
         "distinct": false, 
         "timestamp": "2020-03-26T17:29:47-07:00", 
         "modified": [
            "setup.py", 
            "wafer/compare/templates/admin/wafer.compare/change_form.html", 
            "wafer/pages/templates/wafer.pages/page_form.html", 
            "wafer/settings.py", 
            "wafer/talks/templates/wafer.talks/review_talk.html", 
            "wafer/talks/templates/wafer.talks/talk.html", 
            "wafer/talks/templates/wafer.talks/talk_form.html"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/1afec46cee0e48693457b1fde878832ffd995903", 
         "tree_id": "5d1ab276b36322eb59cf1878d0786cea25f880ee", 
         "message": "Merge pull request #522 from CTPUG/roll-back-markitup-hacks\\n\\nRevert markitup hacks", 
         "removed": [
            "wafer/sponsors/templates/admin/sponsors/change_form.html", 
            "wafer/static/js/markitup.js", 
            "wafer/templates/markitup/editor.html"
         ], 
         "id": "1afec46cee0e48693457b1fde878832ffd995903"
      }, 
      {
         "committer": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "added": [], 
         "author": {
            "name": "Neil Muller", 
            "email": "drnlmuller@gmail.com"
         }, 
         "distinct": true, 
         "timestamp": "2020-04-07T13:04:22+02:00", 
         "modified": [
            "wafer/compare/templates/admin/wafer.compare/change_form.html", 
            "wafer/pages/templates/wafer.pages/page_form.html", 
            "wafer/settings.py", 
            "wafer/talks/templates/wafer.talks/review_talk.html", 
            "wafer/talks/templates/wafer.talks/talk.html", 
            "wafer/talks/templates/wafer.talks/talk_form.html"
         ], 
         "url": "https://github.com/CTPUG/wafer/commit/e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b", 
         "tree_id": "4d2da4fd817dd33d83a2706a7acddfa61db9c853", 
         "message": "Merge branch 'master' into feature/support_django_3", 
         "removed": [
            "wafer/sponsors/templates/admin/sponsors/change_form.html", 
            "wafer/static/js/markitup.js", 
            "wafer/templates/markitup/editor.html"
         ], 
         "id": "e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b"
      }
   ], 
   "after": "e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b", 
   "created": false, 
   "head_commit": {
      "committer": {
         "name": "Neil Muller", 
         "email": "drnlmuller@gmail.com"
      }, 
      "added": [], 
      "author": {
         "name": "Neil Muller", 
         "email": "drnlmuller@gmail.com"
      }, 
      "distinct": true, 
      "timestamp": "2020-04-07T13:04:22+02:00", 
      "modified": [
         "wafer/compare/templates/admin/wafer.compare/change_form.html", 
         "wafer/pages/templates/wafer.pages/page_form.html", 
         "wafer/settings.py", 
         "wafer/talks/templates/wafer.talks/review_talk.html", 
         "wafer/talks/templates/wafer.talks/talk.html", 
         "wafer/talks/templates/wafer.talks/talk_form.html"
      ], 
      "url": "https://github.com/CTPUG/wafer/commit/e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b", 
      "tree_id": "4d2da4fd817dd33d83a2706a7acddfa61db9c853", 
      "message": "Merge branch 'master' into feature/support_django_3", 
      "removed": [
         "wafer/sponsors/templates/admin/sponsors/change_form.html", 
         "wafer/static/js/markitup.js", 
         "wafer/templates/markitup/editor.html"
      ], 
      "id": "e5fbad190b41cd4f79d1321e7c6d8deee67c6a3b"
   }, 
   "organization": {
      "issues_url": "https://api.github.com/orgs/CTPUG/issues", 
      "members_url": "https://api.github.com/orgs/CTPUG/members{/member}", 
      "description": null, 
      "public_members_url": "https://api.github.com/orgs/CTPUG/public_members{/member}", 
      "url": "https://api.github.com/orgs/CTPUG", 
      "events_url": "https://api.github.com/orgs/CTPUG/events", 
      "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
      "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
      "repos_url": "https://api.github.com/orgs/CTPUG/repos", 
      "login": "CTPUG", 
      "id": 647522, 
      "hooks_url": "https://api.github.com/orgs/CTPUG/hooks"
   }, 
   "ref": "refs/heads/feature/support_django_3", 
   "base_ref": null, 
   "before": "c4138096f9b92f312f5b2a661af3dc024db89c08"
}
"""

GITHUB_PR = u"""
{
   "sender": {
      "following_url": "https://api.github.com/users/drnlm/following{/other_user}", 
      "events_url": "https://api.github.com/users/drnlm/events{/privacy}", 
      "avatar_url": "https://avatars2.githubusercontent.com/u/642056?v=4", 
      "url": "https://api.github.com/users/drnlm", 
      "gists_url": "https://api.github.com/users/drnlm/gists{/gist_id}", 
      "html_url": "https://github.com/drnlm", 
      "subscriptions_url": "https://api.github.com/users/drnlm/subscriptions", 
      "node_id": "MDQ6VXNlcjY0MjA1Ng==", 
      "repos_url": "https://api.github.com/users/drnlm/repos", 
      "received_events_url": "https://api.github.com/users/drnlm/received_events", 
      "gravatar_id": "", 
      "starred_url": "https://api.github.com/users/drnlm/starred{/owner}{/repo}", 
      "site_admin": false, 
      "login": "drnlm", 
      "type": "User", 
      "id": 642056, 
      "followers_url": "https://api.github.com/users/drnlm/followers", 
      "organizations_url": "https://api.github.com/users/drnlm/orgs"
   }, 
   "repository": {
      "issues_url": "https://api.github.com/repos/CTPUG/wafer/issues{/number}", 
      "deployments_url": "https://api.github.com/repos/CTPUG/wafer/deployments", 
      "stargazers_count": 33, 
      "forks_url": "https://api.github.com/repos/CTPUG/wafer/forks", 
      "mirror_url": null, 
      "subscription_url": "https://api.github.com/repos/CTPUG/wafer/subscription", 
      "notifications_url": "https://api.github.com/repos/CTPUG/wafer/notifications{?since,all,participating}", 
      "collaborators_url": "https://api.github.com/repos/CTPUG/wafer/collaborators{/collaborator}", 
      "updated_at": "2020-03-27T00:29:51Z", 
      "private": false, 
      "pulls_url": "https://api.github.com/repos/CTPUG/wafer/pulls{/number}", 
      "disabled": false, 
      "issue_comment_url": "https://api.github.com/repos/CTPUG/wafer/issues/comments{/number}", 
      "labels_url": "https://api.github.com/repos/CTPUG/wafer/labels{/name}", 
      "has_wiki": true, 
      "full_name": "CTPUG/wafer", 
      "owner": {
         "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
         "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
         "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
         "url": "https://api.github.com/users/CTPUG", 
         "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
         "html_url": "https://github.com/CTPUG", 
         "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
         "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
         "repos_url": "https://api.github.com/users/CTPUG/repos", 
         "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
         "gravatar_id": "", 
         "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
         "site_admin": false, 
         "login": "CTPUG", 
         "type": "Organization", 
         "id": 647522, 
         "followers_url": "https://api.github.com/users/CTPUG/followers", 
         "organizations_url": "https://api.github.com/users/CTPUG/orgs"
      }, 
      "statuses_url": "https://api.github.com/repos/CTPUG/wafer/statuses/{sha}", 
      "id": 8658619, 
      "keys_url": "https://api.github.com/repos/CTPUG/wafer/keys{/key_id}", 
      "description": "A wafer-thin web application for running small conferences. Built using Django.", 
      "tags_url": "https://api.github.com/repos/CTPUG/wafer/tags", 
      "archived": false, 
      "downloads_url": "https://api.github.com/repos/CTPUG/wafer/downloads", 
      "assignees_url": "https://api.github.com/repos/CTPUG/wafer/assignees{/user}", 
      "contents_url": "https://api.github.com/repos/CTPUG/wafer/contents/{+path}", 
      "has_pages": false, 
      "git_refs_url": "https://api.github.com/repos/CTPUG/wafer/git/refs{/sha}", 
      "open_issues_count": 27, 
      "has_projects": true, 
      "clone_url": "https://github.com/CTPUG/wafer.git", 
      "watchers_count": 33, 
      "git_tags_url": "https://api.github.com/repos/CTPUG/wafer/git/tags{/sha}", 
      "milestones_url": "https://api.github.com/repos/CTPUG/wafer/milestones{/number}", 
      "languages_url": "https://api.github.com/repos/CTPUG/wafer/languages", 
      "size": 1977, 
      "homepage": null, 
      "fork": false, 
      "commits_url": "https://api.github.com/repos/CTPUG/wafer/commits{/sha}", 
      "releases_url": "https://api.github.com/repos/CTPUG/wafer/releases{/id}", 
      "issue_events_url": "https://api.github.com/repos/CTPUG/wafer/issues/events{/number}", 
      "archive_url": "https://api.github.com/repos/CTPUG/wafer/{archive_format}{/ref}", 
      "comments_url": "https://api.github.com/repos/CTPUG/wafer/comments{/number}", 
      "events_url": "https://api.github.com/repos/CTPUG/wafer/events", 
      "contributors_url": "https://api.github.com/repos/CTPUG/wafer/contributors", 
      "html_url": "https://github.com/CTPUG/wafer", 
      "forks": 21, 
      "compare_url": "https://api.github.com/repos/CTPUG/wafer/compare/{base}...{head}", 
      "open_issues": 27, 
      "node_id": "MDEwOlJlcG9zaXRvcnk4NjU4NjE5", 
      "git_url": "git://github.com/CTPUG/wafer.git", 
      "svn_url": "https://github.com/CTPUG/wafer", 
      "merges_url": "https://api.github.com/repos/CTPUG/wafer/merges", 
      "has_issues": true, 
      "ssh_url": "git@github.com:CTPUG/wafer.git", 
      "blobs_url": "https://api.github.com/repos/CTPUG/wafer/git/blobs{/sha}", 
      "git_commits_url": "https://api.github.com/repos/CTPUG/wafer/git/commits{/sha}", 
      "hooks_url": "https://api.github.com/repos/CTPUG/wafer/hooks", 
      "has_downloads": true, 
      "license": {
         "spdx_id": "ISC", 
         "url": "https://api.github.com/licenses/isc", 
         "node_id": "MDc6TGljZW5zZTEw", 
         "name": "ISC License", 
         "key": "isc"
      }, 
      "name": "wafer", 
      "language": "Python", 
      "url": "https://api.github.com/repos/CTPUG/wafer", 
      "created_at": "2013-03-08T19:50:58Z", 
      "watchers": 33, 
      "pushed_at": "2020-04-07T16:44:10Z", 
      "forks_count": 21, 
      "default_branch": "master", 
      "teams_url": "https://api.github.com/repos/CTPUG/wafer/teams", 
      "trees_url": "https://api.github.com/repos/CTPUG/wafer/git/trees{/sha}", 
      "branches_url": "https://api.github.com/repos/CTPUG/wafer/branches{/branch}", 
      "subscribers_url": "https://api.github.com/repos/CTPUG/wafer/subscribers", 
      "stargazers_url": "https://api.github.com/repos/CTPUG/wafer/stargazers"
   }, 
   "number": 523, 
   "pull_request": {
      "deletions": 96, 
      "merge_commit_sha": "a30e64e6571c715cae52f8fdb8533aa81d68f275", 
      "additions": 20, 
      "labels": [], 
      "number": 523, 
      "assignee": null, 
      "mergeable": null, 
      "closed_at": "2020-04-07T16:44:10Z", 
      "requested_reviewers": [], 
      "statuses_url": "https://api.github.com/repos/CTPUG/wafer/statuses/52fba4b0fbdf3d410d0bc37a78cc12fba30fb411", 
      "id": 400206235, 
      "maintainer_can_modify": false, 
      "title": "Feature/support django 3", 
      "comments": 0, 
      "merged_at": "2020-04-07T16:44:10Z", 
      "state": "closed", 
      "draft": false, 
      "changed_files": 21, 
      "_links": {
         "review_comment": {
            "href": "https://api.github.com/repos/CTPUG/wafer/pulls/comments{/number}"
         }, 
         "commits": {
            "href": "https://api.github.com/repos/CTPUG/wafer/pulls/523/commits"
         }, 
         "self": {
            "href": "https://api.github.com/repos/CTPUG/wafer/pulls/523"
         }, 
         "review_comments": {
            "href": "https://api.github.com/repos/CTPUG/wafer/pulls/523/comments"
         }, 
         "html": {
            "href": "https://github.com/CTPUG/wafer/pull/523"
         }, 
         "comments": {
            "href": "https://api.github.com/repos/CTPUG/wafer/issues/523/comments"
         }, 
         "issue": {
            "href": "https://api.github.com/repos/CTPUG/wafer/issues/523"
         }, 
         "statuses": {
            "href": "https://api.github.com/repos/CTPUG/wafer/statuses/52fba4b0fbdf3d410d0bc37a78cc12fba30fb411"
         }
      }, 
      "diff_url": "https://github.com/CTPUG/wafer/pull/523.diff", 
      "issue_url": "https://api.github.com/repos/CTPUG/wafer/issues/523", 
      "body": "With the release of django-markitup 4, we can now also support Django 3. This simplifies our dependency handling as we no longer need to play the \\"last version which worked for us\\" pinning game (at least for a while).\\r\\n\\r\\nThis drops support for Python 2, since Django 3 removes most of the helpers for that, and continuing to support it is therefor painful for not much gain.\\r\\n\\r\\nWe also drop support for Django 1.11, since some of our dependencies no longer support it.", 
      "head": {
         "repo": {
            "issues_url": "https://api.github.com/repos/CTPUG/wafer/issues{/number}", 
            "deployments_url": "https://api.github.com/repos/CTPUG/wafer/deployments", 
            "stargazers_count": 33, 
            "forks_url": "https://api.github.com/repos/CTPUG/wafer/forks", 
            "mirror_url": null, 
            "subscription_url": "https://api.github.com/repos/CTPUG/wafer/subscription", 
            "notifications_url": "https://api.github.com/repos/CTPUG/wafer/notifications{?since,all,participating}", 
            "collaborators_url": "https://api.github.com/repos/CTPUG/wafer/collaborators{/collaborator}", 
            "updated_at": "2020-03-27T00:29:51Z", 
            "private": false, 
            "pulls_url": "https://api.github.com/repos/CTPUG/wafer/pulls{/number}", 
            "disabled": false, 
            "issue_comment_url": "https://api.github.com/repos/CTPUG/wafer/issues/comments{/number}", 
            "labels_url": "https://api.github.com/repos/CTPUG/wafer/labels{/name}", 
            "has_wiki": true, 
            "full_name": "CTPUG/wafer", 
            "owner": {
               "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
               "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
               "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
               "url": "https://api.github.com/users/CTPUG", 
               "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
               "html_url": "https://github.com/CTPUG", 
               "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
               "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
               "repos_url": "https://api.github.com/users/CTPUG/repos", 
               "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
               "gravatar_id": "", 
               "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
               "site_admin": false, 
               "login": "CTPUG", 
               "type": "Organization", 
               "id": 647522, 
               "followers_url": "https://api.github.com/users/CTPUG/followers", 
               "organizations_url": "https://api.github.com/users/CTPUG/orgs"
            }, 
            "statuses_url": "https://api.github.com/repos/CTPUG/wafer/statuses/{sha}", 
            "id": 8658619, 
            "keys_url": "https://api.github.com/repos/CTPUG/wafer/keys{/key_id}", 
            "description": "A wafer-thin web application for running small conferences. Built using Django.", 
            "tags_url": "https://api.github.com/repos/CTPUG/wafer/tags", 
            "archived": false, 
            "downloads_url": "https://api.github.com/repos/CTPUG/wafer/downloads", 
            "assignees_url": "https://api.github.com/repos/CTPUG/wafer/assignees{/user}", 
            "contents_url": "https://api.github.com/repos/CTPUG/wafer/contents/{+path}", 
            "has_pages": false, 
            "git_refs_url": "https://api.github.com/repos/CTPUG/wafer/git/refs{/sha}", 
            "open_issues_count": 27, 
            "has_projects": true, 
            "clone_url": "https://github.com/CTPUG/wafer.git", 
            "watchers_count": 33, 
            "git_tags_url": "https://api.github.com/repos/CTPUG/wafer/git/tags{/sha}", 
            "milestones_url": "https://api.github.com/repos/CTPUG/wafer/milestones{/number}", 
            "languages_url": "https://api.github.com/repos/CTPUG/wafer/languages", 
            "size": 1977, 
            "homepage": null, 
            "fork": false, 
            "commits_url": "https://api.github.com/repos/CTPUG/wafer/commits{/sha}", 
            "releases_url": "https://api.github.com/repos/CTPUG/wafer/releases{/id}", 
            "issue_events_url": "https://api.github.com/repos/CTPUG/wafer/issues/events{/number}", 
            "archive_url": "https://api.github.com/repos/CTPUG/wafer/{archive_format}{/ref}", 
            "comments_url": "https://api.github.com/repos/CTPUG/wafer/comments{/number}", 
            "events_url": "https://api.github.com/repos/CTPUG/wafer/events", 
            "contributors_url": "https://api.github.com/repos/CTPUG/wafer/contributors", 
            "html_url": "https://github.com/CTPUG/wafer", 
            "forks": 21, 
            "compare_url": "https://api.github.com/repos/CTPUG/wafer/compare/{base}...{head}", 
            "open_issues": 27, 
            "node_id": "MDEwOlJlcG9zaXRvcnk4NjU4NjE5", 
            "git_url": "git://github.com/CTPUG/wafer.git", 
            "svn_url": "https://github.com/CTPUG/wafer", 
            "merges_url": "https://api.github.com/repos/CTPUG/wafer/merges", 
            "has_issues": true, 
            "ssh_url": "git@github.com:CTPUG/wafer.git", 
            "blobs_url": "https://api.github.com/repos/CTPUG/wafer/git/blobs{/sha}", 
            "git_commits_url": "https://api.github.com/repos/CTPUG/wafer/git/commits{/sha}", 
            "hooks_url": "https://api.github.com/repos/CTPUG/wafer/hooks", 
            "has_downloads": true, 
            "license": {
               "spdx_id": "ISC", 
               "url": "https://api.github.com/licenses/isc", 
               "node_id": "MDc6TGljZW5zZTEw", 
               "name": "ISC License", 
               "key": "isc"
            }, 
            "name": "wafer", 
            "language": "Python", 
            "url": "https://api.github.com/repos/CTPUG/wafer", 
            "created_at": "2013-03-08T19:50:58Z", 
            "watchers": 33, 
            "pushed_at": "2020-04-07T16:44:10Z", 
            "forks_count": 21, 
            "default_branch": "master", 
            "teams_url": "https://api.github.com/repos/CTPUG/wafer/teams", 
            "trees_url": "https://api.github.com/repos/CTPUG/wafer/git/trees{/sha}", 
            "branches_url": "https://api.github.com/repos/CTPUG/wafer/branches{/branch}", 
            "subscribers_url": "https://api.github.com/repos/CTPUG/wafer/subscribers", 
            "stargazers_url": "https://api.github.com/repos/CTPUG/wafer/stargazers"
         }, 
         "sha": "52fba4b0fbdf3d410d0bc37a78cc12fba30fb411", 
         "ref": "feature/support_django_3", 
         "user": {
            "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
            "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
            "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
            "url": "https://api.github.com/users/CTPUG", 
            "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
            "html_url": "https://github.com/CTPUG", 
            "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
            "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
            "repos_url": "https://api.github.com/users/CTPUG/repos", 
            "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
            "gravatar_id": "", 
            "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
            "site_admin": false, 
            "login": "CTPUG", 
            "type": "Organization", 
            "id": 647522, 
            "followers_url": "https://api.github.com/users/CTPUG/followers", 
            "organizations_url": "https://api.github.com/users/CTPUG/orgs"
         }, 
         "label": "CTPUG:feature/support_django_3"
      }, 
      "commits_url": "https://api.github.com/repos/CTPUG/wafer/pulls/523/commits", 
      "commits": 16, 
      "author_association": "MEMBER", 
      "comments_url": "https://api.github.com/repos/CTPUG/wafer/issues/523/comments", 
      "html_url": "https://github.com/CTPUG/wafer/pull/523", 
      "rebaseable": null, 
      "updated_at": "2020-04-07T16:44:11Z", 
      "node_id": "MDExOlB1bGxSZXF1ZXN0NDAwMjA2MjM1", 
      "user": {
         "following_url": "https://api.github.com/users/drnlm/following{/other_user}", 
         "events_url": "https://api.github.com/users/drnlm/events{/privacy}", 
         "avatar_url": "https://avatars2.githubusercontent.com/u/642056?v=4", 
         "url": "https://api.github.com/users/drnlm", 
         "gists_url": "https://api.github.com/users/drnlm/gists{/gist_id}", 
         "html_url": "https://github.com/drnlm", 
         "subscriptions_url": "https://api.github.com/users/drnlm/subscriptions", 
         "node_id": "MDQ6VXNlcjY0MjA1Ng==", 
         "repos_url": "https://api.github.com/users/drnlm/repos", 
         "received_events_url": "https://api.github.com/users/drnlm/received_events", 
         "gravatar_id": "", 
         "starred_url": "https://api.github.com/users/drnlm/starred{/owner}{/repo}", 
         "site_admin": false, 
         "login": "drnlm", 
         "type": "User", 
         "id": 642056, 
         "followers_url": "https://api.github.com/users/drnlm/followers", 
         "organizations_url": "https://api.github.com/users/drnlm/orgs"
      }, 
      "milestone": null, 
      "requested_teams": [], 
      "locked": false, 
      "merged_by": {
         "following_url": "https://api.github.com/users/drnlm/following{/other_user}", 
         "events_url": "https://api.github.com/users/drnlm/events{/privacy}", 
         "avatar_url": "https://avatars2.githubusercontent.com/u/642056?v=4", 
         "url": "https://api.github.com/users/drnlm", 
         "gists_url": "https://api.github.com/users/drnlm/gists{/gist_id}", 
         "html_url": "https://github.com/drnlm", 
         "subscriptions_url": "https://api.github.com/users/drnlm/subscriptions", 
         "node_id": "MDQ6VXNlcjY0MjA1Ng==", 
         "repos_url": "https://api.github.com/users/drnlm/repos", 
         "received_events_url": "https://api.github.com/users/drnlm/received_events", 
         "gravatar_id": "", 
         "starred_url": "https://api.github.com/users/drnlm/starred{/owner}{/repo}", 
         "site_admin": false, 
         "login": "drnlm", 
         "type": "User", 
         "id": 642056, 
         "followers_url": "https://api.github.com/users/drnlm/followers", 
         "organizations_url": "https://api.github.com/users/drnlm/orgs"
      }, 
      "url": "https://api.github.com/repos/CTPUG/wafer/pulls/523", 
      "mergeable_state": "unknown", 
      "created_at": "2020-04-07T11:23:52Z", 
      "merged": true, 
      "review_comments_url": "https://api.github.com/repos/CTPUG/wafer/pulls/523/comments", 
      "review_comments": 0, 
      "assignees": [], 
      "review_comment_url": "https://api.github.com/repos/CTPUG/wafer/pulls/comments{/number}", 
      "base": {
         "repo": {
            "issues_url": "https://api.github.com/repos/CTPUG/wafer/issues{/number}", 
            "deployments_url": "https://api.github.com/repos/CTPUG/wafer/deployments", 
            "stargazers_count": 33, 
            "forks_url": "https://api.github.com/repos/CTPUG/wafer/forks", 
            "mirror_url": null, 
            "subscription_url": "https://api.github.com/repos/CTPUG/wafer/subscription", 
            "notifications_url": "https://api.github.com/repos/CTPUG/wafer/notifications{?since,all,participating}", 
            "collaborators_url": "https://api.github.com/repos/CTPUG/wafer/collaborators{/collaborator}", 
            "updated_at": "2020-03-27T00:29:51Z", 
            "private": false, 
            "pulls_url": "https://api.github.com/repos/CTPUG/wafer/pulls{/number}", 
            "disabled": false, 
            "issue_comment_url": "https://api.github.com/repos/CTPUG/wafer/issues/comments{/number}", 
            "labels_url": "https://api.github.com/repos/CTPUG/wafer/labels{/name}", 
            "has_wiki": true, 
            "full_name": "CTPUG/wafer", 
            "owner": {
               "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
               "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
               "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
               "url": "https://api.github.com/users/CTPUG", 
               "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
               "html_url": "https://github.com/CTPUG", 
               "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
               "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
               "repos_url": "https://api.github.com/users/CTPUG/repos", 
               "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
               "gravatar_id": "", 
               "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
               "site_admin": false, 
               "login": "CTPUG", 
               "type": "Organization", 
               "id": 647522, 
               "followers_url": "https://api.github.com/users/CTPUG/followers", 
               "organizations_url": "https://api.github.com/users/CTPUG/orgs"
            }, 
            "statuses_url": "https://api.github.com/repos/CTPUG/wafer/statuses/{sha}", 
            "id": 8658619, 
            "keys_url": "https://api.github.com/repos/CTPUG/wafer/keys{/key_id}", 
            "description": "A wafer-thin web application for running small conferences. Built using Django.", 
            "tags_url": "https://api.github.com/repos/CTPUG/wafer/tags", 
            "archived": false, 
            "downloads_url": "https://api.github.com/repos/CTPUG/wafer/downloads", 
            "assignees_url": "https://api.github.com/repos/CTPUG/wafer/assignees{/user}", 
            "contents_url": "https://api.github.com/repos/CTPUG/wafer/contents/{+path}", 
            "has_pages": false, 
            "git_refs_url": "https://api.github.com/repos/CTPUG/wafer/git/refs{/sha}", 
            "open_issues_count": 27, 
            "has_projects": true, 
            "clone_url": "https://github.com/CTPUG/wafer.git", 
            "watchers_count": 33, 
            "git_tags_url": "https://api.github.com/repos/CTPUG/wafer/git/tags{/sha}", 
            "milestones_url": "https://api.github.com/repos/CTPUG/wafer/milestones{/number}", 
            "languages_url": "https://api.github.com/repos/CTPUG/wafer/languages", 
            "size": 1977, 
            "homepage": null, 
            "fork": false, 
            "commits_url": "https://api.github.com/repos/CTPUG/wafer/commits{/sha}", 
            "releases_url": "https://api.github.com/repos/CTPUG/wafer/releases{/id}", 
            "issue_events_url": "https://api.github.com/repos/CTPUG/wafer/issues/events{/number}", 
            "archive_url": "https://api.github.com/repos/CTPUG/wafer/{archive_format}{/ref}", 
            "comments_url": "https://api.github.com/repos/CTPUG/wafer/comments{/number}", 
            "events_url": "https://api.github.com/repos/CTPUG/wafer/events", 
            "contributors_url": "https://api.github.com/repos/CTPUG/wafer/contributors", 
            "html_url": "https://github.com/CTPUG/wafer", 
            "forks": 21, 
            "compare_url": "https://api.github.com/repos/CTPUG/wafer/compare/{base}...{head}", 
            "open_issues": 27, 
            "node_id": "MDEwOlJlcG9zaXRvcnk4NjU4NjE5", 
            "git_url": "git://github.com/CTPUG/wafer.git", 
            "svn_url": "https://github.com/CTPUG/wafer", 
            "merges_url": "https://api.github.com/repos/CTPUG/wafer/merges", 
            "has_issues": true, 
            "ssh_url": "git@github.com:CTPUG/wafer.git", 
            "blobs_url": "https://api.github.com/repos/CTPUG/wafer/git/blobs{/sha}", 
            "git_commits_url": "https://api.github.com/repos/CTPUG/wafer/git/commits{/sha}", 
            "hooks_url": "https://api.github.com/repos/CTPUG/wafer/hooks", 
            "has_downloads": true, 
            "license": {
               "spdx_id": "ISC", 
               "url": "https://api.github.com/licenses/isc", 
               "node_id": "MDc6TGljZW5zZTEw", 
               "name": "ISC License", 
               "key": "isc"
            }, 
            "name": "wafer", 
            "language": "Python", 
            "url": "https://api.github.com/repos/CTPUG/wafer", 
            "created_at": "2013-03-08T19:50:58Z", 
            "watchers": 33, 
            "pushed_at": "2020-04-07T16:44:10Z", 
            "forks_count": 21, 
            "default_branch": "master", 
            "teams_url": "https://api.github.com/repos/CTPUG/wafer/teams", 
            "trees_url": "https://api.github.com/repos/CTPUG/wafer/git/trees{/sha}", 
            "branches_url": "https://api.github.com/repos/CTPUG/wafer/branches{/branch}", 
            "subscribers_url": "https://api.github.com/repos/CTPUG/wafer/subscribers", 
            "stargazers_url": "https://api.github.com/repos/CTPUG/wafer/stargazers"
         }, 
         "sha": "1afec46cee0e48693457b1fde878832ffd995903", 
         "ref": "master", 
         "user": {
            "following_url": "https://api.github.com/users/CTPUG/following{/other_user}", 
            "events_url": "https://api.github.com/users/CTPUG/events{/privacy}", 
            "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
            "url": "https://api.github.com/users/CTPUG", 
            "gists_url": "https://api.github.com/users/CTPUG/gists{/gist_id}", 
            "html_url": "https://github.com/CTPUG", 
            "subscriptions_url": "https://api.github.com/users/CTPUG/subscriptions", 
            "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
            "repos_url": "https://api.github.com/users/CTPUG/repos", 
            "received_events_url": "https://api.github.com/users/CTPUG/received_events", 
            "gravatar_id": "", 
            "starred_url": "https://api.github.com/users/CTPUG/starred{/owner}{/repo}", 
            "site_admin": false, 
            "login": "CTPUG", 
            "type": "Organization", 
            "id": 647522, 
            "followers_url": "https://api.github.com/users/CTPUG/followers", 
            "organizations_url": "https://api.github.com/users/CTPUG/orgs"
         }, 
         "label": "CTPUG:master"
      }, 
      "patch_url": "https://github.com/CTPUG/wafer/pull/523.patch"
   }, 
   "action": "closed", 
   "organization": {
      "issues_url": "https://api.github.com/orgs/CTPUG/issues", 
      "members_url": "https://api.github.com/orgs/CTPUG/members{/member}", 
      "description": null, 
      "public_members_url": "https://api.github.com/orgs/CTPUG/public_members{/member}", 
      "url": "https://api.github.com/orgs/CTPUG", 
      "events_url": "https://api.github.com/orgs/CTPUG/events", 
      "avatar_url": "https://avatars1.githubusercontent.com/u/647522?v=4", 
      "node_id": "MDEyOk9yZ2FuaXphdGlvbjY0NzUyMg==", 
      "repos_url": "https://api.github.com/orgs/CTPUG/repos", 
      "login": "CTPUG", 
      "id": 647522, 
      "hooks_url": "https://api.github.com/orgs/CTPUG/hooks"
   }
}
"""

# pipelines-play test repo
BITBUCKET_GIT_PUSH = u"""
{
   "push": {
      "changes": [
         {
            "forced": false,
            "old": {
               "name": "master",
               "links": {
                  "commits": {
                     "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commits/master"
                  },
                  "self": {
                     "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/refs/branches/master"
                  },
                  "html": {
                     "href": "https://bitbucket.org/drnlm/pipelines_play/branch/master"
                  }
               },
               "default_merge_strategy": "merge_commit",
               "merge_strategies": [
                  "merge_commit",
                  "squash",
                  "fast_forward"
               ],
               "type": "branch",
               "target": {
                  "rendered": {},
                  "hash": "dd0a682744b84f92a8cf3f467603848b9009b0d6",
                  "links": {
                     "self": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                     },
                     "html": {
                        "href": "https://bitbucket.org/drnlm/pipelines_play/commits/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                     }
                  },
                  "author": {
                     "raw": "Neil <drnlmuller@gmail.com>",
                     "type": "author"
                  },
                  "summary": {
                     "raw": "Add pause\\n",
                     "markup": "markdown",
                     "html": "<p>Add pause</p>",
                     "type": "rendered"
                  },
                  "parents": [
                     {
                        "hash": "1b48e5d2507ce7d3f86774c7f1c16a5d3dac2b7a",
                        "type": "commit",
                        "links": {
                           "self": {
                              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/1b48e5d2507ce7d3f86774c7f1c16a5d3dac2b7a"
                           },
                           "html": {
                              "href": "https://bitbucket.org/drnlm/pipelines_play/commits/1b48e5d2507ce7d3f86774c7f1c16a5d3dac2b7a"
                           }
                        }
                     }
                  ],
                  "date": "2019-11-01T08:26:12+00:00",
                  "message": "Add pause\\n",
                  "type": "commit",
                  "properties": {}
               }
            },
            "links": {
               "commits": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commits?include=4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c&exclude=dd0a682744b84f92a8cf3f467603848b9009b0d6"
               },
               "html": {
                  "href": "https://bitbucket.org/drnlm/pipelines_play/branches/compare/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c..dd0a682744b84f92a8cf3f467603848b9009b0d6"
               },
               "diff": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/diff/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c..dd0a682744b84f92a8cf3f467603848b9009b0d6"
               }
            },
            "created": false,
            "commits": [
               {
                  "rendered": {},
                  "hash": "4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c",
                  "links": {
                     "self": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     },
                     "comments": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c/comments"
                     },
                     "patch": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/patch/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     },
                     "html": {
                        "href": "https://bitbucket.org/drnlm/pipelines_play/commits/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     },
                     "diff": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/diff/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     },
                     "approve": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c/approve"
                     },
                     "statuses": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c/statuses"
                     }
                  },
                  "author": {
                     "raw": "Neil <drnlmuller@gmail.com>",
                     "type": "author"
                  },
                  "summary": {
                     "raw": "Update details\\n",
                     "markup": "markdown",
                     "html": "<p>Update details</p>",
                     "type": "rendered"
                  },
                  "parents": [
                     {
                        "hash": "dd0a682744b84f92a8cf3f467603848b9009b0d6",
                        "type": "commit",
                        "links": {
                           "self": {
                              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                           },
                           "html": {
                              "href": "https://bitbucket.org/drnlm/pipelines_play/commits/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                           }
                        }
                     }
                  ],
                  "date": "2020-04-10T10:02:08+00:00",
                  "message": "Update details\\n",
                  "type": "commit",
                  "properties": {}
               }
            ],
            "truncated": false,
            "closed": false,
            "new": {
               "name": "master",
               "links": {
                  "commits": {
                     "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commits/master"
                  },
                  "self": {
                     "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/refs/branches/master"
                  },
                  "html": {
                     "href": "https://bitbucket.org/drnlm/pipelines_play/branch/master"
                  }
               },
               "default_merge_strategy": "merge_commit",
               "merge_strategies": [
                  "merge_commit",
                  "squash",
                  "fast_forward"
               ],
               "type": "branch",
               "target": {
                  "rendered": {},
                  "hash": "4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c",
                  "links": {
                     "self": {
                        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     },
                     "html": {
                        "href": "https://bitbucket.org/drnlm/pipelines_play/commits/4d8aa4adaf1f90b2f20d86f1cb753e898b2fd04c"
                     }
                  },
                  "author": {
                     "raw": "Neil <drnlmuller@gmail.com>",
                     "type": "author"
                  },
                  "summary": {
                     "raw": "Update details\\n",
                     "markup": "markdown",
                     "html": "<p>Update details</p>",
                     "type": "rendered"
                  },
                  "parents": [
                     {
                        "hash": "dd0a682744b84f92a8cf3f467603848b9009b0d6",
                        "type": "commit",
                        "links": {
                           "self": {
                              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                           },
                           "html": {
                              "href": "https://bitbucket.org/drnlm/pipelines_play/commits/dd0a682744b84f92a8cf3f467603848b9009b0d6"
                           }
                        }
                     }
                  ],
                  "date": "2020-04-10T10:02:08+00:00",
                  "message": "Update details\\n",
                  "type": "commit",
                  "properties": {}
               }
            }
         }
      ]
   },
   "actor": {
      "display_name": "Neil Muller",
      "uuid": "{XXXX-XXXX-XXXX-XXXX}",
      "links": {
         "self": {
            "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
         },
         "html": {
            "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
         },
         "avatar": {
            "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
         }
      },
      "nickname": "drnlm",
      "type": "user",
      "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c"
   },
   "repository": {
      "scm": "git",
      "website": "",
      "name": "Pipelines_Play",
      "links": {
         "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play"
         },
         "html": {
            "href": "https://bitbucket.org/drnlm/pipelines_play"
         },
         "avatar": {
            "href": "https://bytebucket.org/ravatar/%7B9f7d92bc-3aad-458e-a11d-0992624e17a9%7D?ts=default"
         }
      },
      "full_name": "drnlm/pipelines_play",
      "owner": {
         "display_name": "Neil Muller",
         "uuid": "{XXXX-XXXX-XXXX-XXXX}",
         "links": {
            "self": {
               "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
            },
            "html": {
               "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
            },
            "avatar": {
               "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
            }
         },
         "nickname": "drnlm",
         "type": "user",
         "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c"
      },
      "type": "repository",
      "is_private": false,
      "uuid": "{XXXX-XXXX-XXXX-XXXX}"
   }
}
"""

BITBUCKET_HG_PUSH = u"""
{
  "push": {
    "changes": [
      {
        "forced": false,
        "old": {
          "heads": [
            {
              "rendered": {},
              "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
              "links": {
                "self": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                },
                "html": {
                  "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                }
              },
              "author": {},
              "summary": {
                "raw": "Add new commit",
                "markup": "markdown",
                "html": "<p>Add new commit</p>",
                "type": "rendered"
              },
              "parents": [
                {
                  "hash": "3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8",
                  "type": "commit",
                  "links": {
                    "self": {
                      "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8"
                    },
                    "html": {
                      "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8"
                    }
                  }
                }
              ],
              "date": "2020-04-10T10:09:46+00:00",
              "message": "Add new commit",
              "type": "commit",
              "properties": {}
            }
          ],
          "name": "default",
          "links": {
            "commits": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits/default"
            },
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/refs/branches/default"
            },
            "html": {
              "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branch/default"
            }
          },
          "default_merge_strategy": "merge_commit",
          "merge_strategies": [
            "merge_commit"
          ],
          "type": "named_branch",
          "target": {
            "rendered": {},
            "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+bitbucket@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{XXXX-XXXX-XXXX-XXXX}"
              }
            },
            "summary": {
              "raw": "Add new commit",
              "markup": "markdown",
              "html": "<p>Add new commit</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/3d6ff418f20fb4c59f1c0701cafbe24caf0e75a8"
                  }
                }
              }
            ],
            "date": "2020-04-10T10:09:46+00:00",
            "message": "Add new commit",
            "type": "commit",
            "properties": {}
          }
        },
        "links": {
          "commits": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits?include=ac1e13ceb3feca459342997b4be8ddc6264ee2f8&exclude=34693836bd2dfecf56cb5a5e8c427c50bae838ca"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branches/compare/ac1e13ceb3feca459342997b4be8ddc6264ee2f8..34693836bd2dfecf56cb5a5e8c427c50bae838ca"
          },
          "diff": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/diff/ac1e13ceb3feca459342997b4be8ddc6264ee2f8..34693836bd2dfecf56cb5a5e8c427c50bae838ca"
          }
        },
        "created": false,
        "commits": [
          {
            "rendered": {},
            "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "comments": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8/comments"
              },
              "patch": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/patch/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "diff": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/diff/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "approve": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8/approve"
              },
              "statuses": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8/statuses"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+bitbucket@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{XXXX-XXXX-XXXX-XXXX}"
              }
            },
            "summary": {
              "raw": "Add new commit",
              "markup": "markdown",
              "html": "<p>Add new commit</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  }
                }
              }
            ],
            "date": "2020-04-10T10:11:10+00:00",
            "message": "Add new commit",
            "type": "commit",
            "properties": {}
          }
        ],
        "truncated": false,
        "closed": false,
        "new": {
          "heads": [
            {
              "rendered": {},
              "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
              "links": {
                "self": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                },
                "html": {
                  "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                }
              },
              "author": {},
              "summary": {
                "raw": "Add new commit",
                "markup": "markdown",
                "html": "<p>Add new commit</p>",
                "type": "rendered"
              },
              "parents": [
                {
                  "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
                  "type": "commit",
                  "links": {
                    "self": {
                      "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                    },
                    "html": {
                      "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                    }
                  }
                }
              ],
              "date": "2020-04-10T10:11:10+00:00",
              "message": "Add new commit",
              "type": "commit",
              "properties": {}
            }
          ],
          "name": "default",
          "links": {
            "commits": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits/default"
            },
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/refs/branches/default"
            },
            "html": {
              "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branch/default"
            }
          },
          "default_merge_strategy": "merge_commit",
          "merge_strategies": [
            "merge_commit"
          ],
          "type": "named_branch",
          "target": {
            "rendered": {},
            "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+bitbucket@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{XXXX-XXXX-XXXX-XXXX}"
              }
            },
            "summary": {
              "raw": "Add new commit",
              "markup": "markdown",
              "html": "<p>Add new commit</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  }
                }
              }
            ],
            "date": "2020-04-10T10:11:10+00:00",
            "message": "Add new commit",
            "type": "commit",
            "properties": {}
          }
        }
      }
    ]
  },
  "actor": {
    "display_name": "Neil Muller",
    "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
      },
      "html": {
        "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
      },
      "avatar": {
        "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
      }
    },
    "nickname": "drnlm",
    "type": "user",
    "uuid": "{XXXX-XXXX-XXXX-XXXX}"
  },
  "repository": {
    "scm": "hg",
    "website": "",
    "name": "bitbucket-service-test",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test"
      },
      "html": {
        "href": "https://bitbucket.org/drnlm/bitbucket-service-test"
      },
      "avatar": {
        "href": "https://bytebucket.org/ravatar/%7B9e1b6aad-767d-4698-ad69-8f48c2b6c041%7D?ts=default"
      }
    },
    "full_name": "drnlm/bitbucket-service-test",
    "owner": {
      "display_name": "Neil Muller",
      "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
        },
        "html": {
          "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
        },
        "avatar": {
          "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
        }
      },
      "nickname": "drnlm",
      "type": "user",
      "uuid": "{XXXX-XXXX-XXXX-XXXX}"
    },
    "type": "repository",
    "is_private": false,
    "uuid": "{XXXX-XXXX-XXXX-XXXX}"
  }
}
"""

# Editing via web on bitbucket
BITBUCKET_WEB_PUSH = """
{
  "push": {
    "changes": [
      {
        "forced": false,
        "old": {
          "heads": [
            {
              "rendered": {},
              "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
              "links": {
                "self": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                },
                "html": {
                  "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                }
              },
              "author": {},
              "summary": {
                "raw": "Add new commit",
                "markup": "markdown",
                "html": "<p>Add new commit</p>",
                "type": "rendered"
              },
              "parents": [
                {
                  "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
                  "type": "commit",
                  "links": {
                    "self": {
                      "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                    },
                    "html": {
                      "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                    }
                  }
                }
              ],
              "date": "2020-04-10T10:11:10+00:00",
              "message": "Add new commit",
              "type": "commit",
              "properties": {}
            }
          ],
          "name": "default",
          "links": {
            "commits": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits/default"
            },
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/refs/branches/default"
            },
            "html": {
              "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branch/default"
            }
          },
          "default_merge_strategy": "merge_commit",
          "merge_strategies": [
            "merge_commit"
          ],
          "type": "named_branch",
          "target": {
            "rendered": {},
            "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+bitbucket@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
              }
            },
            "summary": {
              "raw": "Add new commit",
              "markup": "markdown",
              "html": "<p>Add new commit</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "34693836bd2dfecf56cb5a5e8c427c50bae838ca",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/34693836bd2dfecf56cb5a5e8c427c50bae838ca"
                  }
                }
              }
            ],
            "date": "2020-04-10T10:11:10+00:00",
            "message": "Add new commit",
            "type": "commit",
            "properties": {}
          }
        },
        "links": {
          "commits": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits?include=9f3e4312248b5391aa99c29408ab9a7c79715acd&exclude=ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branches/compare/9f3e4312248b5391aa99c29408ab9a7c79715acd..ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
          },
          "diff": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/diff/9f3e4312248b5391aa99c29408ab9a7c79715acd..ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
          }
        },
        "created": false,
        "commits": [
          {
            "rendered": {},
            "hash": "9f3e4312248b5391aa99c29408ab9a7c79715acd",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              },
              "comments": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd/comments"
              },
              "patch": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/patch/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              },
              "diff": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/diff/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              },
              "approve": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd/approve"
              },
              "statuses": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd/statuses"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+github@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
              }
            },
            "summary": {
              "raw": "data edited online with Bitbucket",
              "markup": "markdown",
              "html": "<p>data edited online with Bitbucket</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                  }
                }
              }
            ],
            "date": "2020-04-10T15:07:18+00:00",
            "message": "data edited online with Bitbucket",
            "type": "commit",
            "properties": {}
          }
        ],
        "truncated": false,
        "closed": false,
        "new": {
          "heads": [
            {
              "rendered": {},
              "hash": "9f3e4312248b5391aa99c29408ab9a7c79715acd",
              "links": {
                "self": {
                  "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd"
                },
                "html": {
                  "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/9f3e4312248b5391aa99c29408ab9a7c79715acd"
                }
              },
              "author": {},
              "summary": {
                "raw": "data edited online with Bitbucket",
                "markup": "markdown",
                "html": "<p>data edited online with Bitbucket</p>",
                "type": "rendered"
              },
              "parents": [
                {
                  "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
                  "type": "commit",
                  "links": {
                    "self": {
                      "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                    },
                    "html": {
                      "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                    }
                  }
                }
              ],
              "date": "2020-04-10T15:07:18+00:00",
              "message": "data edited online with Bitbucket",
              "type": "commit",
              "properties": {}
            }
          ],
          "name": "default",
          "links": {
            "commits": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commits/default"
            },
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/refs/branches/default"
            },
            "html": {
              "href": "https://bitbucket.org/drnlm/bitbucket-service-test/branch/default"
            }
          },
          "default_merge_strategy": "merge_commit",
          "merge_strategies": [
            "merge_commit"
          ],
          "type": "named_branch",
          "target": {
            "rendered": {},
            "hash": "9f3e4312248b5391aa99c29408ab9a7c79715acd",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              },
              "html": {
                "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/9f3e4312248b5391aa99c29408ab9a7c79715acd"
              }
            },
            "author": {
              "raw": "Neil Muller <drnlmuller+github@gmail.com>",
              "type": "author",
              "user": {
                "display_name": "Neil Muller",
                "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
                  },
                  "html": {
                    "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
                  },
                  "avatar": {
                    "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
                  }
                },
                "nickname": "drnlm",
                "type": "user",
                "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
              }
            },
            "summary": {
              "raw": "data edited online with Bitbucket",
              "markup": "markdown",
              "html": "<p>data edited online with Bitbucket</p>",
              "type": "rendered"
            },
            "parents": [
              {
                "hash": "ac1e13ceb3feca459342997b4be8ddc6264ee2f8",
                "type": "commit",
                "links": {
                  "self": {
                    "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test/commit/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                  },
                  "html": {
                    "href": "https://bitbucket.org/drnlm/bitbucket-service-test/commits/ac1e13ceb3feca459342997b4be8ddc6264ee2f8"
                  }
                }
              }
            ],
            "date": "2020-04-10T15:07:18+00:00",
            "message": "data edited online with Bitbucket",
            "type": "commit",
            "properties": {}
          }
        }
      }
    ]
  },
  "actor": {
    "display_name": "Neil Muller",
    "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
      },
      "html": {
        "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
      },
      "avatar": {
        "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
      }
    },
    "nickname": "drnlm",
    "type": "user",
    "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
  },
  "repository": {
    "scm": "hg",
    "website": "",
    "name": "bitbucket-service-test",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/bitbucket-service-test"
      },
      "html": {
        "href": "https://bitbucket.org/drnlm/bitbucket-service-test"
      },
      "avatar": {
        "href": "https://bytebucket.org/ravatar/%7B9e1b6aad-767d-4698-ad69-8f48c2b6c041%7D?ts=default"
      }
    },
    "full_name": "drnlm/bitbucket-service-test",
    "owner": {
      "display_name": "Neil Muller",
      "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
        },
        "html": {
          "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
        },
        "avatar": {
          "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
        }
      },
      "nickname": "drnlm",
      "type": "user",
      "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
    },
    "type": "repository",
    "is_private": false,
    "uuid": "{9e1b6aad-767d-4698-ad69-8f48c2b6c041}"
  }
}
"""

BITBUCKET_PR = u"""
{
  "pullrequest": {
    "rendered": {
      "description": {
        "raw": "Testing PR's created and merged hooks",
        "markup": "markdown",
        "html": "<p>Testing PR's created and merged hooks</p>",
        "type": "rendered"
      },
      "title": {
        "raw": "Add message",
        "markup": "markdown",
        "html": "<p>Add message</p>",
        "type": "rendered"
      }
    },
    "type": "pullrequest",
    "description": "Testing PR's created and merged hooks",
    "links": {
      "decline": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/decline"
      },
      "diffstat": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/diffstat/drnlm/pipelines_play:4e0b047f23fa%0D4d8aa4adaf1f?from_pullrequest_id=1"
      },
      "commits": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/commits"
      },
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1"
      },
      "comments": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/comments"
      },
      "merge": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/merge"
      },
      "html": {
        "href": "https://bitbucket.org/drnlm/pipelines_play/pull-requests/1"
      },
      "activity": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/activity"
      },
      "diff": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/diff/drnlm/pipelines_play:4e0b047f23fa%0D4d8aa4adaf1f?from_pullrequest_id=1"
      },
      "approve": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/approve"
      },
      "statuses": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/pullrequests/1/statuses"
      }
    },
    "title": "Add message",
    "close_source_branch": true,
    "reviewers": [],
    "id": 1,
    "destination": {
      "commit": {
        "hash": "4d8aa4adaf1f",
        "type": "commit",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4d8aa4adaf1f"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/pipelines_play/commits/4d8aa4adaf1f"
          }
        }
      },
      "repository": {
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/pipelines_play"
          },
          "avatar": {
            "href": "https://bytebucket.org/ravatar/%7B9f7d92bc-3aad-458e-a11d-0992624e17a9%7D?ts=default"
          }
        },
        "type": "repository",
        "name": "Pipelines_Play",
        "full_name": "drnlm/pipelines_play",
        "uuid": "{9f7d92bc-3aad-458e-a11d-0992624e17a9}"
      },
      "branch": {
        "name": "master"
      }
    },
    "created_on": "2020-04-10T10:26:42.962118+00:00",
    "summary": {
      "raw": "Testing PR's created and merged hooks",
      "markup": "markdown",
      "html": "<p>Testing PR's created and merged hooks</p>",
      "type": "rendered"
    },
    "source": {
      "commit": {
        "hash": "4e0b047f23fa",
        "type": "commit",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play/commit/4e0b047f23fa"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/pipelines_play/commits/4e0b047f23fa"
          }
        }
      },
      "repository": {
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play"
          },
          "html": {
            "href": "https://bitbucket.org/drnlm/pipelines_play"
          },
          "avatar": {
            "href": "https://bytebucket.org/ravatar/%7B9f7d92bc-3aad-458e-a11d-0992624e17a9%7D?ts=default"
          }
        },
        "type": "repository",
        "name": "Pipelines_Play",
        "full_name": "drnlm/pipelines_play",
        "uuid": "{9f7d92bc-3aad-458e-a11d-0992624e17a9}"
      },
      "branch": {
        "name": "test_pr"
      }
    },
    "comment_count": 0,
    "state": "OPEN",
    "task_count": 0,
    "participants": [],
    "reason": "",
    "updated_on": "2020-04-10T10:26:43.000315+00:00",
    "author": {
      "display_name": "Neil Muller",
      "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
        },
        "html": {
          "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
        },
        "avatar": {
          "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
        }
      },
      "nickname": "drnlm",
      "type": "user",
      "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
    },
    "merge_commit": null,
    "closed_by": null
  },
  "repository": {
    "scm": "git",
    "website": "",
    "name": "Pipelines_Play",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/drnlm/pipelines_play"
      },
      "html": {
        "href": "https://bitbucket.org/drnlm/pipelines_play"
      },
      "avatar": {
        "href": "https://bytebucket.org/ravatar/%7B9f7d92bc-3aad-458e-a11d-0992624e17a9%7D?ts=default"
      }
    },
    "full_name": "drnlm/pipelines_play",
    "owner": {
      "display_name": "Neil Muller",
      "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
        },
        "html": {
          "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
        },
        "avatar": {
          "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
        }
      },
      "nickname": "drnlm",
      "type": "user",
      "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
    },
    "type": "repository",
    "is_private": false,
    "uuid": "{9f7d92bc-3aad-458e-a11d-0992624e17a9}"
  },
  "actor": {
    "display_name": "Neil Muller",
    "account_id": "557058:989e08b9-3d38-49a4-8b6b-24c565dc810c",
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/users/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D"
      },
      "html": {
        "href": "https://bitbucket.org/%7B44a4f383-520e-4a5b-8fb3-7616ff04a87c%7D/"
      },
      "avatar": {
        "href": "https://secure.gravatar.com/avatar/d2c5efdb0e3d876a335d4ee99342f464?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FNM-1.png"
      }
    },
    "nickname": "drnlm",
    "type": "user",
    "uuid": "{44a4f383-520e-4a5b-8fb3-7616ff04a87c}"
  }
}
"""

# Gitlab from test repo

GITLAB_PUSH = u"""
{
  "object_kind": "push",
  "event_name": "push",
  "before": "70145ac405e23b7c581d88f80ff5acf2efc73f21",
  "after": "40a75a8d6010540fcdec81e1107b33dfff51af3b",
  "ref": "refs/heads/master",
  "checkout_sha": "40a75a8d6010540fcdec81e1107b33dfff51af3b",
  "message": null,
  "user_id": 135447,
  "user_name": "Neil Muller",
  "user_username": "drnlmza",
  "user_email": "",
  "user_avatar": "https://secure.gravatar.com/avatar/4836486929fe8c8aafb57de6217daebe?s=80&d=identicon",
  "project_id": 385769,
  "project": {
    "id": 385769,
    "name": "test",
    "description": "A test project",
    "web_url": "https://gitlab.com/drnlmza/test",
    "avatar_url": null,
    "git_ssh_url": "git@gitlab.com:drnlmza/test.git",
    "git_http_url": "https://gitlab.com/drnlmza/test.git",
    "namespace": "Neil Muller",
    "visibility_level": 0,
    "path_with_namespace": "drnlmza/test",
    "default_branch": "master",
    "ci_config_path": null,
    "homepage": "https://gitlab.com/drnlmza/test",
    "url": "git@gitlab.com:drnlmza/test.git",
    "ssh_url": "git@gitlab.com:drnlmza/test.git",
    "http_url": "https://gitlab.com/drnlmza/test.git"
  },
  "commits": [
    {
      "id": "45faa2054a0a9bc3021fb6f690533752286cfd2e",
      "message": "do\\n",
      "title": "do",
      "timestamp": "2020-04-12T15:04:40+02:00",
      "url": "https://gitlab.com/drnlmza/test/-/commit/45faa2054a0a9bc3021fb6f690533752286cfd2e",
      "author": {
        "name": "Neil Muller",
        "email": "drnlmuller+gitorious@gmail.com"
      },
      "added": [

      ],
      "modified": [
        "README"
      ],
      "removed": [

      ]
    },
    {
      "id": "e9ae55a0e356b3aa3572abcc69ce98859ba8bd73",
      "message": "undo\\n",
      "title": "undo",
      "timestamp": "2020-04-12T15:04:46+02:00",
      "url": "https://gitlab.com/drnlmza/test/-/commit/e9ae55a0e356b3aa3572abcc69ce98859ba8bd73",
      "author": {
        "name": "Neil Muller",
        "email": "drnlmuller+gitorious@gmail.com"
      },
      "added": [

      ],
      "modified": [
        "README"
      ],
      "removed": [

      ]
    },
    {
      "id": "40a75a8d6010540fcdec81e1107b33dfff51af3b",
      "message": "re-do?\\n",
      "title": "re-do?",
      "timestamp": "2020-04-12T15:04:59+02:00",
      "url": "https://gitlab.com/drnlmza/test/-/commit/40a75a8d6010540fcdec81e1107b33dfff51af3b",
      "author": {
        "name": "Neil Muller",
        "email": "drnlmuller+gitorious@gmail.com"
      },
      "added": [

      ],
      "modified": [
        "README"
      ],
      "removed": [

      ]
    }
  ],
  "total_commits_count": 3,
  "push_options": {
  },
  "repository": {
    "name": "test",
    "url": "git@gitlab.com:drnlmza/test.git",
    "description": "A test project",
    "homepage": "https://gitlab.com/drnlmza/test",
    "git_http_url": "https://gitlab.com/drnlmza/test.git",
    "git_ssh_url": "git@gitlab.com:drnlmza/test.git",
    "visibility_level": 0
  }
}
"""

# gitlab uses "merge request", but we stick to naming this PR for consitency with the others
GITLAB_PR = u"""
{
  "object_kind": "merge_request",
  "event_type": "merge_request",
  "user": {
    "name": "Neil Muller",
    "username": "drnlmza",
    "avatar_url": "https://secure.gravatar.com/avatar/4836486929fe8c8aafb57de6217daebe?s=80&d=identicon",
    "email": "drnlmuller+gitorious@gmail.com"
  },
  "project": {
    "id": 385769,
    "name": "test",
    "description": "A test project",
    "web_url": "https://gitlab.com/drnlmza/test",
    "avatar_url": null,
    "git_ssh_url": "git@gitlab.com:drnlmza/test.git",
    "git_http_url": "https://gitlab.com/drnlmza/test.git",
    "namespace": "Neil Muller",
    "visibility_level": 0,
    "path_with_namespace": "drnlmza/test",
    "default_branch": "master",
    "ci_config_path": null,
    "homepage": "https://gitlab.com/drnlmza/test",
    "url": "git@gitlab.com:drnlmza/test.git",
    "ssh_url": "git@gitlab.com:drnlmza/test.git",
    "http_url": "https://gitlab.com/drnlmza/test.git"
  },
  "object_attributes": {
    "assignee_id": null,
    "author_id": 135447,
    "created_at": "2020-04-10 11:32:25 UTC",
    "description": "Creat and merge a PR",
    "head_pipeline_id": null,
    "id": 55363964,
    "iid": 1,
    "last_edited_at": null,
    "last_edited_by_id": null,
    "merge_commit_sha": null,
    "merge_error": null,
    "merge_params": {
      "force_remove_source_branch": "1"
    },
    "merge_status": "unchecked",
    "merge_user_id": null,
    "merge_when_pipeline_succeeds": false,
    "milestone_id": null,
    "source_branch": "test_merge",
    "source_project_id": 385769,
    "state_id": 1,
    "target_branch": "master",
    "target_project_id": 385769,
    "time_estimate": 0,
    "title": "Add data",
    "updated_at": "2020-04-10 11:32:25 UTC",
    "updated_by_id": null,
    "url": "https://gitlab.com/drnlmza/test/-/merge_requests/1",
    "source": {
      "id": 385769,
      "name": "test",
      "description": "A test project",
      "web_url": "https://gitlab.com/drnlmza/test",
      "avatar_url": null,
      "git_ssh_url": "git@gitlab.com:drnlmza/test.git",
      "git_http_url": "https://gitlab.com/drnlmza/test.git",
      "namespace": "Neil Muller",
      "visibility_level": 0,
      "path_with_namespace": "drnlmza/test",
      "default_branch": "master",
      "ci_config_path": null,
      "homepage": "https://gitlab.com/drnlmza/test",
      "url": "git@gitlab.com:drnlmza/test.git",
      "ssh_url": "git@gitlab.com:drnlmza/test.git",
      "http_url": "https://gitlab.com/drnlmza/test.git"
    },
    "target": {
      "id": 385769,
      "name": "test",
      "description": "A test project",
      "web_url": "https://gitlab.com/drnlmza/test",
      "avatar_url": null,
      "git_ssh_url": "git@gitlab.com:drnlmza/test.git",
      "git_http_url": "https://gitlab.com/drnlmza/test.git",
      "namespace": "Neil Muller",
      "visibility_level": 0,
      "path_with_namespace": "drnlmza/test",
      "default_branch": "master",
      "ci_config_path": null,
      "homepage": "https://gitlab.com/drnlmza/test",
      "url": "git@gitlab.com:drnlmza/test.git",
      "ssh_url": "git@gitlab.com:drnlmza/test.git",
      "http_url": "https://gitlab.com/drnlmza/test.git"
    },
    "last_commit": {
      "id": "a9d7f36140a2b0e5c4658dc19e8e7266c30d8db7",
      "message": "Add data\\n",
      "title": "Add data",
      "timestamp": "2020-04-10T13:31:51+02:00",
      "url": "https://gitlab.com/drnlmza/test/-/commit/a9d7f36140a2b0e5c4658dc19e8e7266c30d8db7",
      "author": {
        "name": "Neil Muller",
        "email": "drnlmuller+gitorious@gmail.com"
      }
    },
    "work_in_progress": false,
    "total_time_spent": 0,
    "human_total_time_spent": null,
    "human_time_estimate": null,
    "assignee_ids": [

    ],
    "state": "opened",
    "action": "open"
  },
  "labels": [

  ],
  "changes": {
    "author_id": {
      "previous": null,
      "current": 135447
    },
    "created_at": {
      "previous": null,
      "current": "2020-04-10 11:32:25 UTC"
    },
    "description": {
      "previous": null,
      "current": "Creat and merge a PR"
    },
    "id": {
      "previous": null,
      "current": 55363964
    },
    "iid": {
      "previous": null,
      "current": 1
    },
    "merge_params": {
      "previous": {
      },
      "current": {
        "force_remove_source_branch": "1"
      }
    },
    "source_branch": {
      "previous": null,
      "current": "test_merge"
    },
    "source_project_id": {
      "previous": null,
      "current": 385769
    },
    "target_branch": {
      "previous": null,
      "current": "master"
    },
    "target_project_id": {
      "previous": null,
      "current": 385769
    },
    "title": {
      "previous": null,
      "current": "Add data"
    },
    "updated_at": {
      "previous": null,
      "current": "2020-04-10 11:32:25 UTC"
    },
    "total_time_spent": {
      "previous": null,
      "current": 0
    }
  },
  "repository": {
    "name": "test",
    "url": "git@gitlab.com:drnlmza/test.git",
    "description": "A test project",
    "homepage": "https://gitlab.com/drnlmza/test"
  }
}
"""

SF_PUSH = """{
   "repository": {
      "url": "https://sourceforge.net/p/sutekh/sutekh/",
      "name": "Git Repository",
      "full_name": "/p/sutekh/sutekh/"
   },
   "commits": [
      {
         "committer": {
            "username": "",
            "name": "Neil Muller",
            "email": "drnlmuller+sutekh@gmail.com"
         },
         "added": [],
         "author": {
            "username": "",
            "name": "Neil Muller",
            "email": "drnlmuller+sutekh@gmail.com"
         },
         "url": "https://sourceforge.net/p/sutekh/sutekh/ci/948c0ca080104658e64f90ecc0b3c6ddf80a32c1/",
         "timestamp": "2020-11-05T08:22:44Z",
         "modified": [
            "sutekh",
            "sutekh/sutekh",
            "sutekh/sutekh/base",
            "sutekh/sutekh/base/gui",
            "sutekh/sutekh/base/gui/SutekhDialog.py",
            "sutekh/sutekh/gui",
            "sutekh/sutekh/gui/Test.py"
         ],
         "renamed": [],
         "message": "Add helper for info dialogs",
         "removed": [],
         "id": "948c0ca080104658e64f90ecc0b3c6ddf80a32c1",
         "copied": []
      }
   ],
   "after": "948c0ca080104658e64f90ecc0b3c6ddf80a32c1",
   "size": 1,
   "ref": "refs/heads/master",
   "before": "ba1b3e869b05a1ea88a6f925eb5e27f68a15f284"
}
"""
